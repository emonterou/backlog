import django_tables2 as tables
from apps.models import Cliente

class ClienteTable(tables.Table):
    class Meta:
        model = Cliente
        # add class="paleblue" to <table> tag
        attrs = {"class": "paleblue"}