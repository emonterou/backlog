var region_arr = new Array("Arica y Parinacota", "Tarapacá", "Antofagasta", "Atacama", "Coquimbo", "Valparaíso",
 "Region Metropolitana", "Libertador Bernardo Ohiggins", "Maule", "Biobío", "Araucanía", "Los Ríos", "Los Lagos",
 "Aysén", "Magallanes");

var s_a = new Array();
s_a[0] = "";
s_a[1] = "Arica|Camarones|Putre|General Lagos";
s_a[2] = "Iquique|Alto Hospicio|Pozo Almonte|Camiña|Colchane|Huara|Pica";
s_a[3] = "Antofagasta|Mejillones|Sierra Gorda|Taltal|Calama|Ollagüe|San Pedro de Atacama|Tocopilla|María Elena";

s_a[4] = "Copiapó|Caldera|Tierra Amarilla|Chañaral|Diego de Almagro|Vallenar|Alto del Carmen|Freirina|Huasco";

s_a[5] = "La Serena|Coquimbo|Andacollo|La Higuera|Paiguano|Vicuña|Illapel|Canela|Los Vilos|Salamanca|Ovalle|Combarbalá|Monte Patria|Punitaqui|Río Hurtado";

s_a[6] = "Valparaíso|Casablanca|Concón|Juan Fernández|Puchuncaví|Quintero|Viña del Mar|Isla de Pascua|Los Andes|Calle Larga|Rinconada|San Esteban|La Ligua|Cabildo|Papudo|Petorca|Zapallar|Quillota|Calera|Hijuelas|La Cruz|Nogales|San Antonio|Algarrobo|Cartagena|El Quisco|El Tabo|Santo Domingo|San Felipe|Catemu|Llaillay|Panquehue|Putaendo|Limache|Quilpué|Villa Alemana|Olmué|Santa María";

s_a[7] = "Santiago|Cerrillos|Cerro Navia|Conchalí|El Bosque|Estación Central|Huechuraba|Independencia|La Cisterna|La Florida|La Granja|La Pintana|La Reina|Las Condes|Lo Barnechea|Lo Espejo|Lo Prado|Macul|Maipú|Ñuñoa|Pedro Aguirre Cerda|Peñalolén|Providencia|Pudahuel|Quilicura|Quinta Normal|Recoleta|Renca|San Joaquín|San Miguel|San Ramón|Vitacura|Puente Alto|Pirque|San José de Maipo|Colina|Lampa|Tiltil|San Bernardo|Buin|Calera de Tango|Paine|Melipilla|Alhué|Curacaví|María Pinto|San Pedro|Talagante|El Monte|Isla de Maipo|Padre Hurtado|Peñaflor";

s_a[8] = "Rancagua|Codegua|Coinco|Coltauco|Doñihue|Graneros|Las Cabras|Machalí|Malloa|Mostazal|Olivar|Peumo|Pichidegua|Quinta de Tilcoco|Rengo|Requínoa|San Vicente|Pichilemu|La Estrella|Litueche|Marchihue|Navidad|Paredones|San Fernando|Chépica|Chimbarongo|Lolol|Rancagua|Palmilla|Peralillo|Placilla|Pumanque|Santa Cruz";
s_a[9] = "Talca|Constitución|Curepto|Empedrado|Maule|Pelarco|Pencahue|Río Claro|San Clemente|San Rafael|Cauquenes|Chanco|Pelluhue|Hualañé|Licantén|Molina|Rauco|Romeral|Sagrada Familia|Teno|Vichuquén|Linares|Colbún|Longaví|Parral|Retiro|San Javier|Villa Alegre|Yerbas Buenas";
s_a[10] = "Concepción|Coronel|Chiguayante|Florida|Hualqui|Lota|Penco|San Pedro de la Paz|Santa Juana|Talcahuano|Tomé|Hualpén|Lebu|Arauco|Cañete|Contulmo|Curanilahue|Los Alamos|Tirúa|Los Angeles|Antuco|Cabrero|Laja|Mulchén|Nacimiento|Negrete|Quilaco|Quilleco|San Rosendo|Santa Bárbara|Tucapel|Yumbel|Alto Biobío|Chillán|Bulnes|Cobquecura|Coelemu|Coihueco|Chillán Viejo|El Carmen|Ninhue|Ñiquén|Pemuco|Pinto|Portezuelo|Quillón|Quirihue|Ránquil|San Carlos|San Fabián|San Ignacio|San Nicolás|Treguaco|Yungay";
s_a[11] = "Temuco|Carahue|Cunco|Curarrehue|Freire|Galvarino|Gorbea|Lautaro|Loncoche|Melipeuco|Nueva Imperial|Padre Las Casas|Perquenco|Pitrufquén|Pucón|Saavedra|Teodoro Schmidt|Toltén|Vilcún|Villarrica|Cholchol|Angol|Collipulli|Curacautín|Ercilla|Lonquimay|Los Sauces|Lumaco|Purén|Renaico|Traiguén|Victoria";
s_a[12] = "Valdivia|Corral|Lanco|Los Lagos|Máfil|Mariquina|Paillaco|Panguipulli|La Unión|Futrono|Lago Ranco|Río Bueno";
s_a[13] = "Puerto Montt|Calbuco|Cochamó|Fresia|Frutillar|Los Muermos|Llanquihue|Maullín|Puerto Varas|Castro|Ancud|Chonchi|Curaco de Vélez|Dalcahue|Puqueldón|Queilén|Quellón|Quemchi|Quinchao|Osorno|Puerto Octay|Purranque|Puyehue|Río Negro|San Juan de la Costa|San Pablo|Chaitén|Futaleufú|Hualaihué|Palena";
s_a[14] = "Coihaique|Lago Verde|Aisén|Cisnes|Guaitecas|Cochrane|O'Higgins|Tortel|Chile Chico|Río Ibáñez";
s_a[15] = "Punta Arenas|Laguna Blanca|Río Verde|San Gregorio|Cabo de Hornos|Antártica|Porvenir|Primavera|Timaukel|Natales|Torres del Paine";

function poblarComuna(regionElementId, comunaElementId) {

    var selectedregionIndex = document.getElementById(regionElementId).selectedIndex;

    var comunaElement = document.getElementById(comunaElementId);

    comunaElement.length = 0; // Fixed by Julian Woods
    comunaElement.options[0] = new Option('Selecciona comuna', '');
    comunaElement.selectedIndex = 0;

    var comuna_arr = s_a[selectedregionIndex].split("|");

    for (var i = 0; i < comuna_arr.length; i++) {
        comunaElement.options[comunaElement.length] = new Option(comuna_arr[i], comuna_arr[i]);
    }
}

function poblarRegion(regionElementId, comunaElementId) {
    // given the id of the <select> tag as function argument, it inserts <option> tags
    var regionElement = document.getElementById(regionElementId);
    regionElement.length = 0;
    regionElement.options[0] = new Option('Selecciona region', '');
    regionElement.selectedIndex = 0;
    for (var i = 0; i < region_arr.length; i++) {
        regionElement.options[regionElement.length] = new Option(region_arr[i], region_arr[i]);
    }

    // Assigned all countries. Now assign event listener for the comunas.

    if (comunaElementId) {
        regionElement.onchange = function () {
            poblarComuna(regionElementId, comunaElementId);
        };
    }
}