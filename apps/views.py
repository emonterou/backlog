# -*- coding: utf-8 -*-
import json
import unicodedata
import os.path, time, datetime, calendar
import xlwt
from django.http import *
from django.db.models import Q
from django.shortcuts import render_to_response,redirect,get_object_or_404, render
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.views import login, logout_then_login
from apps.form import ClienteForm, HitoForm, NegocioForm, NotaVentaForm, MinegocioForm
from django.db import models
from models import Cliente, Hito, Negocio, NotaVenta, EjecutivoComercial, Participacion, LogHitos, DocumentoTributario, DocumentoTributarioHito
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _
from django.core import serializers
from datetime import datetime, date, timedelta

def filtra(rut):
    """Funcion verifica que un rut sea valido. """    
    caracteres = "1234567890k"
    rutx = ""
    for cambio in rut.lower():
        if cambio in caracteres:
            rutx += cambio
    return rutx

def valida(rut):
    rfiltro = filtra(rut)
    rutx = str(rfiltro[0:len(rfiltro)-1])
    digito = str(rfiltro[-1])
    multiplo = 2
    total = 0
    for reverso in reversed(rutx):
        total += int(reverso) * multiplo
        if multiplo == 7:
            multiplo = 2
        else:
            multiplo += 1
        modulus = total % 11
        verificador = 11 - modulus
        if verificador == 10:
            div = "k"
        elif verificador == 11:
            div = "0"
        else:
            if verificador < 10:
                div = verificador
    if str(div) == str(digito):
        retorno = "Valido"
    else:
        retorno = "Invalido"
    return retorno

def cant_notificaciones():
    """ Calcula la cantidad de notificaciones que existirań en base a las que no esten vistas y sean del tipo Cambio de Fecha y Cambio de Monto. """    
    if LogHitos.objects.filter((Q(tipo__contains="Cambio de Fecha") | Q(tipo__contains="Cambio de Monto")), estado=False).exists:
        size_log = LogHitos.objects.filter((Q(tipo__contains="Cambio de Fecha") | Q(tipo__contains="Cambio de Monto")), estado=False).count()
    else:
        size_log = None    
    return size_log    

def logout_then_login(request):
    """ Cerrar Sesion. """    
    logout(request)
    return render_to_response('login.html', context_instance=RequestContext(request))

def login_user(request):
    """ Login para los usuarios del sistema. """    
    if request.user.is_authenticated():
        return HttpResponseRedirect('/index/')
    else:    
        logout(request)
        username = password = ''
        if request.POST:
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/index/')
        return render_to_response('login.html', context_instance=RequestContext(request))
    
@login_required(login_url='/login/')
def clientepragma(request):
    """ Funcion relacionada con la vista de cliente.html, envia los objetos necesarios que se muestran en esta vista, ultimos dos 
    usuarios y negocios ingresados, clientes, cantidad de notificaciones y Ejecutivos Comerciales. """        
    size_log = cant_notificaciones()
    ult_clientes = None
    ult_negocios = None
    clientes = Cliente.objects.all()
    ejecutivos = EjecutivoComercial.objects.all()
    if Cliente.objects.order_by('-id').exists:
        ult_clientes = Cliente.objects.order_by('-id')[:2]
    if Negocio.objects.order_by('-id').exists:
        ult_negocios = Negocio.objects.order_by('-id')[:2]
    return render_to_response('cliente.html',  {'size_log': size_log, 'ejecutivos': ejecutivos,'ult_clientes': ult_clientes, 'ult_negocios': ult_negocios, 'clientes': clientes, 'errores': "" , 'errornegocio': ""} , context_instance=RequestContext(request))

def devolver_lista(hito):
    """ Devuelve una lista a partir de un hito en un formato especifico: [Id Hito, Fecha de Hito, Monto UF de Hito, Descripcion de Hito,
    Nota de Venta Asociada, Negocio Asociado, Cliente Asociado] """    
    lista_hitos = [] 
    monto_acumulado= 0 
    for h in hito:    
        info_NV = NotaVenta.objects.filter(pk=h.notaVenta_id_id)
        info_negocio = Negocio.objects.filter(pk=info_NV[0].negocio_id_id)
        info_cliente = Cliente.objects.filter(pk=info_negocio[0].cliente_id_id)
        monto_acumulado = h.montoUF + monto_acumulado
        lista_hitos.append([h.id, h.fecha,h.estado,h.montoUF,h.descripcion, info_NV[0].numeroNotaVenta, info_negocio[0].nombreNegocio, info_cliente[0].nombreFantasia])
    return lista_hitos, monto_acumulado

def devolver_lista_balance(hito):
    """ Devuelve una lista en base a un hito al igual que la funcion devolver_lista pero agregando los totales por mes del
    hito. """    
    lista_hitos = [] 
    monto_acumulado= 0
    montoMes = 0
    i = 0
    for h in hito: 
        fecha_hito = h.fecha        
        if i == 0:
            mes_anterior = fecha_hito.month 
        info_NV = NotaVenta.objects.filter(pk=h.notaVenta_id_id)
        info_negocio = Negocio.objects.filter(pk=info_NV[0].negocio_id_id)
        info_cliente = Cliente.objects.filter(pk=info_negocio[0].cliente_id_id)
        monto_acumulado = h.montoUF + monto_acumulado 

        if fecha_hito.month == mes_anterior:
            lista_hitos.append([h.id, h.fecha,h.estado,h.montoUF,h.descripcion, info_NV[0].numeroNotaVenta, info_negocio[0].nombreNegocio, info_cliente[0].nombreFantasia])          
            montoMes = montoMes + h.montoUF
        else:
            lista_hitos.append([" ", " ", " ", montoMes, "Total Mes:", " ", " ", " "])
            lista_hitos.append([h.id, h.fecha,h.estado,h.montoUF,h.descripcion, info_NV[0].numeroNotaVenta, info_negocio[0].nombreNegocio, info_cliente[0].nombreFantasia])          
            montoMes = h.montoUF
        i = i+1

        mes_anterior = fecha_hito.month 
    lista_hitos.append([" ", " ", " ", montoMes, "Total Mes: ", " ", " ", " "])
    lista_hitos.append([" ", " ", " ", monto_acumulado, "Total: ", " ", " ", " "])
    if len(lista_hitos) == 2:
        lista_hitos = []          
    return lista_hitos

@login_required(login_url='/login/')
def index(request):
    """ Funcion asociada a la vista index.html, envia los objetos que se mostraran, tales como los 2 ultimos hitos facturados
    durante la semana y todos los hitos pendientes por facturar, ademas de los montos acumulados de los hitos facturados y
    los pendientes de facturar, ademas de la cantidad de notificaciones."""    
    size_log = cant_notificaciones()
    #Estado 5 corresponde a los hitos facturados
    semana_ant = date.today()-timedelta(days=7)
    if Hito.objects.filter(estado=5, fecha__range=[semana_ant, date.today()]).reverse()[:2].exists():
        hitoFacturados = Hito.objects.filter(estado=5, fecha__range=[semana_ant, date.today()]).reverse()[:2]
    else:
        hitoFacturados =  Hito.objects.filter(estado=5).reverse()[:2]
    hitoNoFacturados = Hito.objects.exclude(estado=5).reverse()
    lista_hitos, monto_acumulado_Fac = devolver_lista(hitoFacturados)    
    lista_hitos_NF, monto_acumulado_NF = devolver_lista(hitoNoFacturados)
    return render_to_response('index.html', {'size_log': size_log, 'monto_acumulado_NF': monto_acumulado_NF,'monto_acumulado_Fac': monto_acumulado_Fac, 'lista_hitos_NF': lista_hitos_NF,'lista_hitos': lista_hitos}, context_instance=RequestContext(request))

@login_required(login_url='/login/')
def notaVenta(request):
    """ Funcion asociada con la vista notaVenta.html, envia los objetos que se mostraran, los clientes, ejecutivos comerciales, 
    cantidad de notificaciones. """    
    size_log = cant_notificaciones()  
    clientes = Cliente.objects.all()
    ejecutivos = EjecutivoComercial.objects.all()
    return render_to_response('notaVenta.html',  {'size_log': size_log, 'clientes' : clientes , 'notaVentas': None, 'ejecutivos': ejecutivos} , context_instance=RequestContext(request))

@login_required(login_url='/login/')
def hitos(request):
    """ Funcion asociada con la vista hitos.html, envia los objetos que se mostraran, los clientes y cantidad de notificaciones. """    
    size_log = cant_notificaciones()     
    clientes = Cliente.objects.all()
    return render_to_response('hitos.html',{'size_log': size_log, 'clientes': clientes , 'hitos': None }, context_instance=RequestContext(request))    

@login_required(login_url='/login/')
def balance(request):
    """ Funcion asociada con la vista balance.html, envia los objetos que se mostraran, los clientes, cantidad de notificaciones. """    
    clientes = Cliente.objects.all() 
    size_log = cant_notificaciones() 
    return render_to_response('balance.html',  {'size_log': size_log, 'clientes': clientes } , context_instance=RequestContext(request))

@login_required(login_url='/login/')
def notificaciones(request):
    """ Funcion asociada con la vista notificaciones.html, envia los objetos que se mostraran, la lista de hitos pendientes
    por aprobar o rechazar en un formato especifico, con los clientes, negocio y Nota de Venta asociados. """    
    lista = []
    if LogHitos.objects.filter((Q(tipo__contains="Cambio de Fecha") | Q(tipo__contains="Cambio de Monto")), estado=False).exists():
        logHitos = LogHitos.objects.filter((Q(tipo__contains="Cambio de Fecha") | Q(tipo__contains="Cambio de Monto")), estado=False).order_by('fecha')
        for logHito in logHitos:
            hito = Hito.objects.filter(pk=logHito.hito_id)
            if hito:
                notaVenta = NotaVenta.objects.filter(pk=hito[0].notaVenta_id_id)
                negocio = Negocio.objects.filter(pk=notaVenta[0].negocio_id_id)
                cliente = Cliente.objects.filter(pk=negocio[0].cliente_id_id) 
                lista.append([logHito.fecha, cliente[0].nombreFantasia, negocio[0].nombreNegocio, notaVenta[0].numeroNotaVenta,hito[0].id,logHito.tipo, hito[0].fecha, logHito.fechaEdicion, hito[0].montoUF, logHito.montoUFEdicion, logHito.id])
            else:
                notaVenta = NotaVenta.objects.filter(pk=logHito.notaVenta_id)    
                negocio = Negocio.objects.filter(pk=notaVenta[0].negocio_id_id)
                cliente = Cliente.objects.filter(pk=negocio[0].cliente_id_id) 
                lista.append([logHito.fecha, cliente[0].nombreFantasia, negocio[0].nombreNegocio, notaVenta[0].numeroNotaVenta,logHito.hito_id,logHito.tipo, logHito.fechaHito, logHito.fechaEdicion, logHito.montoUF, logHito.montoUFEdicion, logHito.id])
    else:
        lista = None
    return render_to_response('notificaciones.html',  { 'lista': lista } , context_instance=RequestContext(request))

@login_required(login_url='/login/')
def logHitos(request):
    """ Funcion asociada con la vista logHitos.html, envia la lista de todos los cambios realizados a los hitos(ingresar Hito, 
    Editar Hito(comentatios, descripcion y estado), Cambio de Fecha, Cambio de Monto, Eliminacion de Hito) ordenados por
    Nota de Venta. """
    lista = []
    size_log = cant_notificaciones()  
    notaVentas = NotaVenta.objects.all()
    if LogHitos.objects.all().exists():
        logHitos = LogHitos.objects.all().order_by('notaVenta_id')
        nota = logHitos[0].notaVenta_id
        for logHito in logHitos:
            if nota != logHito.notaVenta_id:
                lista.append(['','','','','','','','','','',''])
            notaVenta = NotaVenta.objects.filter(pk=logHito.notaVenta_id)    
            negocio = Negocio.objects.filter(pk=notaVenta[0].negocio_id_id)
            cliente = Cliente.objects.filter(pk=negocio[0].cliente_id_id) 
            nota = logHito.notaVenta_id
            if logHito.estado == True:
                estado = "Gestionado"
            elif logHito.estado == False and (logHito.tipo == "Cambio de Fecha" or logHito.tipo == "Cambio de Monto"):
                estado = "En espera"    
            else:
                estado= "No aplica"    
            lista.append([logHito.fecha, cliente[0].nombreFantasia, negocio[0].nombreNegocio, notaVenta[0].numeroNotaVenta,logHito.hito_id,logHito.tipo, logHito.fechaHito, logHito.fechaEdicion, logHito.montoUF, logHito.montoUFEdicion, logHito.id, estado])
    else:
        lista = None    
    return render_to_response('logHitos.html',  { 'lista': lista, 'size_log': size_log, 'notaVentas': notaVentas } , context_instance=RequestContext(request))

def ingresar_negocio(request):
    """ Ingreso de Negocio mediante el formulario. """
    size_log = cant_notificaciones()  
    ult_clientes = None
    ult_negocios = None
    try:
        clientes = Cliente.objects.all()
        ejecutivos = EjecutivoComercial.objects.all()
        if request.POST:
            fecha = '-'.join(reversed(request.POST['fechaNegocio'].split('/')))
            negocio = Negocio()
            cliente = Cliente(id=request.POST['cliente_id'])
            negocio.cliente_id = cliente
            negocio.idOportunidadZoho = request.POST['idOportunidadZoho']
            negocio.nombreNegocio = request.POST['nombreNegocio']
            negocio.fechaNegocio = fecha
            negocio.descripcion = request.POST['descripcion']
            negocio.idejecutivoComercial = request.POST['idejecutivoComercial']
            negocio.save()
            exito_negocio = "El negocio fue guardado exitosamente"
            if Cliente.objects.order_by('-id').exists:
                ult_clientes = Cliente.objects.order_by('-id')[:2]
            if Negocio.objects.order_by('-id').exists:
                ult_negocios = Negocio.objects.order_by('-id')[:2]                  
            return render_to_response('cliente.html', {'exito': exito_negocio, 'size_log': size_log, 'ejecutivos': ejecutivos, 'clientes': clientes, 'ult_clientes': ult_clientes,'ult_negocios': ult_negocios}, context_instance=RequestContext(request)) 
    except:
        if Cliente.objects.order_by('-id').exists:
            ult_clientes = Cliente.objects.order_by('-id')[:2]
        if Negocio.objects.order_by('-id').exists:
            ult_negocios = Negocio.objects.order_by('-id')[:2]   
        return render_to_response('cliente.html', {'error': 'Ocurrio un error al ingresar el Negocio, intentelo nuevamente.', 'size_log': size_log, 'ejecutivos': ejecutivos, 'clientes': clientes, 'ult_clientes': ult_clientes,'ult_negocios': ult_negocios}, context_instance=RequestContext(request)) 

def ingresar_clientepragma(request):
    """ Ingreso de Cliente mediante el formulario. """
    size_log = cant_notificaciones() 
    clientes = Cliente.objects.all() 
    ejecutivos = EjecutivoComercial.objects.all()   
    if request.POST:
        try:
            cliente = Cliente()
            cliente.rut = request.POST['rut']
            cliente.razonSocial = request.POST['razonSocial']
            cliente.nombreFantasia = request.POST['nombreFantasia']
            cliente.grupoEconomico = request.POST['grupoEconomico']
            cliente.giro = request.POST['giro']
            cliente.telefono = request.POST['telefono']
            cliente.fax = request.POST['fax']
            cliente.direccion = request.POST['direccion']
            cliente.sector = request.POST['sector']
            cliente.idCuentaZoho = request.POST['idCuentaZoho']
            cliente.region = request.POST['region']
            cliente.comuna= request.POST['comuna']     
            cliente.activo = True
            cliente.save()
            exito_cliente = "El cliente se ha guardado exitosamente"
            ult_clientes = Cliente.objects.all().reverse()[:2]
            ult_negocios = Negocio.objects.all().reverse()[:2]
            return render_to_response('cliente.html', {'exito': exito_cliente, 'size_log': size_log, 'ejecutivos': ejecutivos,'clientes': clientes, 'ult_clientes': ult_clientes, 'ult_negocios': ult_negocios}, context_instance=RequestContext(request))
        except:
            ult_clientes = Cliente.objects.all().reverse()[:2]
            ult_negocios = Negocio.objects.all().reverse()[:2]
            return render_to_response('cliente.html', {'error': 'Ocurrio un error al ingresar el Cliente, intentelo nuevamente.', 'size_log': size_log, 'ejecutivos': ejecutivos,'clientes': clientes, 'ult_clientes': ult_clientes, 'ult_negocios': ult_negocios}, context_instance=RequestContext(request))
                           
def ingresar_NV(request):
    """ Ingreso de Nota de Venta mediante el formulario. """
    size_log = cant_notificaciones()   
    clientes = Cliente.objects.all()
    ejecutivos = EjecutivoComercial.objects.all()
    if request.POST:
        try:
            fecha = '-'.join(reversed(request.POST['fechaNotaVenta'].split('/')))
            notaVenta = NotaVenta()
            participacion = Participacion()
            montoUF = float(unicode(request.POST['montoUF']))
            tipoCambioUF = float(unicode(request.POST['tipoCambioUF']))        
            tipoCambioDivisa = float(unicode(request.POST['tipoCambioDivisa']))

            negocio = Negocio(id=request.POST['negocio'])
            notaVenta.negocio_id = negocio
            notaVenta.numeroNotaVenta = request.POST ['numeroNotaVenta']
            notaVenta.fechaNotaVenta = fecha
            notaVenta.montoUF = request.POST ['montoUF']
            notaVenta.tipoCambioUF = request.POST ['tipoCambioUF']
            notaVenta.divisa = request.POST ['divisa']
            notaVenta.tipoCambioDivisa = request.POST ['tipoCambioDivisa']
            notaVenta.monedaOriginal = request.POST ['monedaOriginal']
            notaVenta.nombreServicio = request.POST ['nombreServicio']
            notaVenta.tipoServicio = request.POST ['tipoServicio']
            notaVenta.subTipoServicio = request.POST ['subTipoServicio']
            notaVenta.responsableDelivery = request.POST ['responsableDelivery']
            notaVenta.correoRespDelivery= request.POST ['correoRespDelivery']
            notaVenta.margenNegocioUF = request.POST ['margenNegocioUF']
            notaVenta.margenNegocioPercent = request.POST ['margenNegocioPercent']
            notaVenta.glosa = request.POST ['glosa']
            montoCL = montoUF * tipoCambioUF
            montoDivisa = tipoCambioDivisa* montoCL
            notaVenta.montoCL = montoCL
            notaVenta.montoDivisa = montoDivisa
            notaVenta.instFacturacion = request.POST ['instFacturacion']
            notaVenta.linkNotaVenta ="No"
            notaVenta.linkOC = "No"
            notaVenta.linkPropuesta = "No"
            notaVenta.linkEstimacion = "No"
            notaVenta.estado = "Activo"
            notaVenta.save()
            ejecutivoComercial = EjecutivoComercial(id=request.POST['ejecutivoComercial'])
            participacion.notaVenta_id = notaVenta
            participacion.ejecutivoComercial_id = ejecutivoComercial
            participacion.porcentaje = request.POST ['porcentaje']  
            participacion.save()
            exito_NV = "La Nota de Venta fue guardada exitosamente"
            return render_to_response('agregarHito.html', {'exito_NV': exito_NV, 'size_log': size_log, 'clientes': clientes, 'notaVentas': None, 'ejecutivos': ejecutivos }, context_instance=RequestContext(request))
        except:
            return render_to_response('agregarHito.html', {'error_NV': 'Ocurrio un error al guardar la Nota de Venta, intentelo nuevamente.', 'size_log': size_log, 'clientes': clientes, 'notaVentas': None, 'ejecutivos': ejecutivos }, context_instance=RequestContext(request)) 

def buscar_cliente(request):
    """ Busca los clientes que contengan la palabra ingresada por el usuario, devolviendo una lista de clientes. """
    size_log = cant_notificaciones() 
    ult_clientes = Cliente.objects.all().reverse()[:2]
    ult_negocios = Negocio.objects.all().reverse()[:2] 
    clientes = Cliente.objects.all()
    ejecutivos = EjecutivoComercial.objects.all()   
    if request.POST:
        cliente = request.POST.get('cliente_a_buscar', None)
        clientes_a_buscar = Cliente.objects.filter(Q(nombreFantasia__icontains=cliente))
        return render_to_response('cliente.html',{'clientes': clientes, 'ejecutivos': ejecutivos, 'size_log': size_log, 'clientes_a_buscar': clientes_a_buscar, 'ult_clientes': ult_clientes, 'ult_negocios': ult_negocios}, context_instance=RequestContext(request) )

def buscar_negocio(request):
    """ Busca los negocios que contengan la palabra ingresada por el usuario, devolviendo una lsista de negocios. """
    size_log = cant_notificaciones() 

    ult_clientes = Cliente.objects.all().reverse()[:2]
    ult_negocios = Negocio.objects.all().reverse()[:2] 
    clientes = Cliente.objects.all()
    ejecutivos = EjecutivoComercial.objects.all()   
    if request.POST:
        negocio = request.POST.get('negocio_a_buscar', None)
        negocios_a_buscar = Negocio.objects.filter(Q(nombreNegocio__icontains=negocio))
        return render_to_response('cliente.html',{'clientes': clientes, 'ejecutivos': ejecutivos, 'size_log': size_log, 'negocios_a_buscar': negocios_a_buscar, 'ult_clientes': ult_clientes, 'ult_negocios': ult_negocios}, context_instance=RequestContext(request) )

def auth_validate(request):
    """ Valida que el rut ingresado por el usuario no exista en la base de datos, si no existe llama a la funcion valida(rut)
    para verificar que el rut sea correcto. """    
    error = ''
    success = False
    if request.method == 'POST':
        rut = request.POST.get('rut', None)
        datos = rut.split('=')
        dat = datos[1]
        mensaje = ''
        if not dat or dat is None:
            error = _('Por favor, ingresa un RUT')
        elif Cliente.objects.filter(rut__exact=dat).exists():
            mensaje = 'si'           
        else:
            mensaje = valida(rut)
    return HttpResponse(mensaje,mimetype='application/javascript')

def auth_validateId(request):
    """ Valida el idOportunidadZoho para que este sea unico. """    
    if request.method == 'POST':
        idOportunidadZoho = request.POST.get('idOportunidadZoho', None)
        datos = idOportunidadZoho.split('=')
        dat = datos[1]
        mensaje = ''
        if not idOportunidadZoho or idOportunidadZoho is None:
            mensaje = 'ingrese'
        elif Negocio.objects.filter(idOportunidadZoho__exact=dat).exists():
            mensaje = 'si'           
        else:
            mensaje = 'no'
    return HttpResponse(mensaje,mimetype='application/javascript')  

def auth_validateNV(request):
    """ Valida el numeroNotaVenta sea unico. """
    if request.method == 'POST':
        numeroNotaVenta = request.POST.get('numeroNotaVenta', None)
        datos = numeroNotaVenta.split('=')
        dat = datos[1]
        mensaje = ''
        if not numeroNotaVenta or numeroNotaVenta is None:
            mensaje = 'ingrese'
        elif NotaVenta.objects.filter(numeroNotaVenta__exact=dat).exists():
            mensaje = 'si'           
        else:
            mensaje = 'no'
    return HttpResponse(mensaje,mimetype='application/javascript')     

def eliminar_cliente(request):
    """ Elimina el cliente solamente si no tiene negocios asociados."""
    size_log = cant_notificaciones() 
    ult_clientes = None
    ult_negocios = None
    clientes = Cliente.objects.all()
    ejecutivos = EjecutivoComercial.objects.all()
    if Cliente.objects.order_by('-id').exists:
        ult_clientes = Cliente.objects.order_by('-id')[:2]
    if Negocio.objects.order_by('-id').exists:
        ult_negocios = Negocio.objects.order_by('-id')[:2]    
    if request.POST:
        try:
            rut_cliente= request.POST['eliminarCliente']
            cliente= Cliente.objects.get(rut__exact=rut_cliente)
            if Negocio.objects.filter(cliente_id_id__exact= cliente.id).exists() == False:
                cliente = Cliente.objects.get(rut__exact=rut_cliente).delete()
                exito_eliminarCliente = "El Cliente fue eliminado exitosamente"
                return render_to_response('cliente.html', {'exito': exito_eliminarCliente, 'size_log': size_log, 'ejecutivos': ejecutivos,'ult_negocios' : ult_negocios ,'clientes' : clientes, 'ult_clientes' : ult_clientes  ,'errores' : errores }, context_instance=RequestContext(request)) 
            else:
                errores = "No puedes borrar este Cliente ya que tiene Negocios asociados"  
                return render_to_response('cliente.html', {'error': errores, 'size_log': size_log, 'ejecutivos': ejecutivos,'ult_negocios' : ult_negocios ,'clientes' : clientes, 'ult_clientes' : ult_clientes  ,'errores' : errores }, context_instance=RequestContext(request)) 
        except:        
            return render_to_response('cliente.html', {'error':'Ocurrio un problema al eliminar el Cliente, intentelo nuevamente', 'size_log': size_log, 'ejecutivos': ejecutivos,'ult_negocios' : ult_negocios ,'clientes' : clientes, 'ult_clientes' : ult_clientes  ,'errores' : errores }, context_instance=RequestContext(request)) 

def enviar_rut(request):
    """ A partir del rut se envia el objeto cliente asoaciado a este rut. """
    rut = request.POST.get('idComparacion', None)
    c = Cliente.objects.get(rut=rut)
    datajson = serializers.serialize('json', [c])
    response = HttpResponse(datajson,
        content_type='application/json'
    )
    return response  

def editar_cliente(request):
    """ Funcion que edita el cliente a partir de los datos ingresados por el usuario en el formulario. """
    size_log = cant_notificaciones() 
    exito_editarCliente = None
    ult_clientes = None
    ult_negocios = None
    clientes = Cliente.objects.all()
    ejecutivos = EjecutivoComercial.objects.all()
    if Cliente.objects.order_by('-id').exists:
        ult_clientes = Cliente.objects.order_by('-id')[:2]
    if Negocio.objects.order_by('-id').exists:
        ult_negocios = Negocio.objects.order_by('-id')[:2]    
    if request.POST:
        try:
            Cliente.objects.filter(rut=request.POST['cliente_rut']).update(razonSocial=request.POST ['razonSocialEditar'], 
            nombreFantasia=request.POST ['nombreFantasiaEditar'], giro = request.POST ['giroEditar'],grupoEconomico = request.POST ['grupoEconomicoEditar'],
            sector = request.POST ['sectorEditar'], idCuentaZoho= request.POST ['idCuentaZohoEditar'],
            telefono = request.POST ['telefonoEditar'],fax = request.POST ['faxEditar'],
            direccion = request.POST ['direccionEditar'])
            exito_editarCliente = "El cliente ha sido editado exitosamente"
            return render_to_response('cliente.html',{'exito':exito_editarCliente, 'size_log': size_log, 'ejecutivos': ejecutivos,'clientes': clientes, 'ult_negocios': ult_negocios, 'ult_clientes': ult_clientes}, context_instance=RequestContext(request))    
        except:
            return render_to_response('cliente.html',{'error':'Ocurrio un error al editar el Cliente, intentelo nuevamente.', 'size_log': size_log, 'ejecutivos': ejecutivos,'clientes': clientes, 'ult_negocios': ult_negocios, 'ult_clientes': ult_clientes}, context_instance=RequestContext(request))    

def eliminar_negocio(request):  
    """ Funcion que elimina el negocio solamente si no tiene Nota de Venta asociadas. """
    size_log = cant_notificaciones()   
    ult_clientes = None
    ult_negocios = None
    clientes = Cliente.objects.all()
    ejecutivos = EjecutivoComercial.objects.all()
    if Cliente.objects.order_by('-id').exists:
        ult_clientes = Cliente.objects.order_by('-id')[:2]
    if Negocio.objects.order_by('-id').exists:
        ult_negocios = Negocio.objects.order_by('-id')[:2] 
    if request.POST:
        try:
            if NotaVenta.objects.filter(negocio_id_id__exact=request.POST['eliminarNegocio']).exists() == False:
                negocio = Negocio.objects.get(pk=request.POST['eliminarNegocio']).delete()
                exito_eliminarNegocio = "El Negocio fue eliminado exitosamente"
                return render_to_response('cliente.html', {'exito': exito_eliminarNegocio, 'size_log': size_log, 'ejecutivos': ejecutivos,'ult_clientes': ult_clientes, 'clientes' : clientes, 'ult_negocios' : ult_negocios}, context_instance=RequestContext(request))        
            else:
                errornegocio = "No puedes borrar este Negocio ya que tiene Notas de Ventas asociadas"
                return render_to_response('cliente.html', {'error': errornegocio, 'size_log': size_log, 'ejecutivos': ejecutivos,'ult_clientes': ult_clientes, 'clientes' : clientes, 'ult_negocios' : ult_negocios}, context_instance=RequestContext(request))        
        except:
                return render_to_response('cliente.html', {'error': 'Ocurrio un error al eliminar el Negocio, intentelo nuevamente', 'size_log': size_log, 'ejecutivos': ejecutivos,'ult_clientes': ult_clientes, 'clientes' : clientes, 'ult_negocios' : ult_negocios}, context_instance=RequestContext(request))        

def enviar_negocio(request):
    """ A partir del id del negocio se envia el objeto negocio. """
    negocio_id = request.POST.get('idComparacion', None)
    n = Negocio.objects.get(pk=negocio_id)
    datajson = serializers.serialize('json', [n])
    response = HttpResponse(datajson,
        content_type='application/json'
    )
    return response  

def editar_negocio(request):
    """ Funcion que edita el negocio a partir de los datos ingresados por el usuario en el formulario. """
    size_log = cant_notificaciones()  
    exito_editarNegocio = None
    ult_clientes = None
    ult_negocios = None
    clientes = Cliente.objects.all()
    ejecutivos = EjecutivoComercial.objects.all()
    if Cliente.objects.order_by('-id').exists:
        ult_clientes = Cliente.objects.order_by('-id')[:2]
    if Negocio.objects.order_by('-id').exists:
        ult_negocios = Negocio.objects.order_by('-id')[:2]    
    if request.POST:
        try:
            negocio = Negocio.objects.get(pk=request.POST['negocio_id_editar'])
            Negocio.objects.filter(pk=request.POST['negocio_id_editar']).update(nombreNegocio=request.POST ['nombreNegocioEditar'], 
            descripcion=request.POST ['descripcionEditar'], fechaNegocio = request.POST ['fechaNegocioEditar'],idejecutivoComercial = request.POST ['idejecutivoComercialEditar'])
            exito_editarNegocio = "El negocio ha sido editado exitosamente"
            return render_to_response('cliente.html',{'exito': exito_editarNegocio, 'size_log': size_log, 'ejecutivos': ejecutivos,'clientes': clientes, 'ult_negocios': ult_negocios, 'ult_clientes': ult_clientes}, context_instance=RequestContext(request)) 
        except:
            return render_to_response('cliente.html',{'error': 'Ocurrio un error al editar el Negocio, intentelo nuevamente.', 'size_log': size_log, 'ejecutivos': ejecutivos,'clientes': clientes, 'ult_negocios': ult_negocios, 'ult_clientes': ult_clientes}, context_instance=RequestContext(request)) 

def buscar_NV(request):
    """ Busca la Nota de Venta a partir de los filtros indicados por el usuario en el buscador de Nota de Venta, si no 
    hay filtro se envian todas las Notas de Venta. """
    notaVentas = NotaVenta()
    notaVentaVacia = None
    clientes = Cliente.objects.all()
    size_log = cant_notificaciones()    
    if request.POST:
        cliente_id = request.POST.get('cliente',None)
        negocio_id = request.POST.get('negocio', None)
        numeroNotaVenta = request.POST['numeroNV']
        fechaNegocioDesde = '-'.join(reversed(request.POST['fechaNegocioDesde'].split('/')))
        fechaNegocioHasta = '-'.join(reversed(request.POST['fechaNegocioHasta'].split('/')))
        if numeroNotaVenta:
            notaVentas = NotaVenta.objects.filter(numeroNotaVenta__exact=numeroNotaVenta)
        elif fechaNegocioDesde and fechaNegocioHasta and negocio_id:
            notaVentas = NotaVenta.objects.filter(negocio_id_id__exact=negocio_id,fechaNotaVenta__range=[fechaNegocioDesde, fechaNegocioHasta])
        elif fechaNegocioDesde and fechaNegocioHasta:
            notaVentas = NotaVenta.objects.filter(fechaNotaVenta__range=[fechaNegocioDesde, fechaNegocioHasta])
        elif negocio_id:
            notaVentas = NotaVenta.objects.filter(negocio_id_id__exact=negocio_id)
        elif fechaNegocioDesde:
            notaVentas = NotaVenta.objects.filter(fechaNotaVenta__gte=fechaNegocioDesde)
        elif fechaNegocioHasta:
            notaVentas = NotaVenta.objects.filter(fechaNotaVenta__lte=fechaNegocioHasta)            
        else:
            notaVentas = NotaVenta.objects.all()   
        if not notaVentas:
            notaVentaVacia = "No se encontraron Notas de Venta bajo este filtro"    
    return render_to_response('notaVenta.html', {'notaVentaVacia': notaVentaVacia, 'size_log': size_log, 'notaVentas': notaVentas, 'clientes': clientes },context_instance=RequestContext(request))    

def enviar_idNV(request):
    """ A partir del id de la Nota de Venta se envia el objeto Nota de Venta. """
    notaVentaId = request.POST.get('idComparacion', None)
    n = NotaVenta.objects.get(pk=notaVentaId)
    datajson = serializers.serialize('json', [n])
    response = HttpResponse(datajson,
        content_type='application/json'
    )
    return response  

def editar_NV(request):
    """ Funcion que edita la Nota de Venta a partir de los datos ingresados por el usuario en el formulario. """
    size_log = cant_notificaciones() 
    clientes = Cliente.objects.all()    
    if request.POST:
        try:
            fecha = '-'.join(reversed(request.POST['fechaNotaVentaEditar'].split('/')))        
            NotaVenta.objects.filter(pk=request.POST['notaVenta_id']).update(fechaNotaVenta=fecha, montoUF=request.POST ['montoUFEditar'], 
            divisa=request.POST ['divisaEditar'], nombreServicio = request.POST ['nombreServicioEditar'],tipoServicio = request.POST ['tipoServicioEditar'],
            responsableDelivery = request.POST ['responsableDeliveryEditar'], correoRespDelivery= request.POST ['correoRespDeliveryEditar'],
            margenNegocioUF = request.POST ['margenNegocioUFEditar'], margenNegocioPercent = request.POST ['margenNegocioPercentEditar'],
            glosa = request.POST ['glosaEditar'], instFacturacion = request.POST ['instFacturacionEditar'])
            exito = "La Nota de Venta fue editada exitosamente."
            return render_to_response('notaVenta.html',{'exito': exito, 'size_log': size_log, 'notaVentas': None , 'clientes': clientes}, context_instance=RequestContext(request))    
        except:
            return render_to_response('notaVenta.html',{'error': 'Ocurrio un error al editar la Nota de Venta, intentelo nuevamente', 'size_log': size_log, 'notaVentas': None , 'clientes': clientes}, context_instance=RequestContext(request))    

def eliminar_NV(request):
    """ Elimina la Nota de Venta solamente si no tiene hitos asoaciados. """
    size_log = cant_notificaciones() 
    clientes = Cliente.objects.all()
    if request.POST:
        try:
            if Hito.objects.filter(notaVenta_id_id__exact=request.POST['eliminarNV']).exists() == False:
                notaVenta = NotaVenta.objects.get(pk=request.POST['eliminarNV']).delete()
                exito = "La Nota de Venta fue eliminada exitosamente."
                return render_to_response('notaVenta.html', {'exito': exito, 'size_log': size_log, 'clientes' : clientes }, context_instance=RequestContext(request))                
            else:
                error = "No puedes borrar esta Nota de Venta ya que tiene hitos asociados."
                return render_to_response('notaVenta.html', {'error': error, 'size_log': size_log, 'clientes' : clientes }, context_instance=RequestContext(request))                
        except:
                error = "Ocurrio un error al eliminar la Nota de Venta, intentelo nuevamente."
                return render_to_response('notaVenta.html', {'error': error, 'size_log': size_log, 'clientes' : clientes }, context_instance=RequestContext(request))                

def poblar_negocio(request):
    """ A partir de un cliente se crea una lista con todos los negocios asociados. """
    qs = Negocio.objects.filter(cliente_id_id__exact=request.GET['q']).values('pk', 'nombreNegocio')
    response = HttpResponse(
        content=json.dumps(list(qs)),
        content_type='application/json'
    )
    return response

def poblar_NV(request):
    """ A partir de un negocio se crea una lista con todos las Notas de Venta asociadas. """
    qs = NotaVenta.objects.filter(negocio_id_id__exact=request.GET['q']).values('pk', 'numeroNotaVenta')
    response = HttpResponse(
        content=json.dumps(list(qs)),
        content_type='application/json'
    )
    return response

def llenarLogHitos(hito_id):
    """ Funcion que crea el objeto logHito a partir de los datos que hay en el hito que tiene asoaciado devolviendo 
    un objeto del tipo logHito. """
    hito = Hito.objects.get(pk=hito_id)
    logHito = LogHitos()
    logHito.hito_id = hito.id
    logHito.fecha = date.today()
    logHito.estadoHito = hito.estado
    logHito.fechaHito = hito.fecha #Fecha con la que viene el hito 
    logHito.comentarios = hito.comentarios
    logHito.descripcion = hito.descripcion
    logHito.montoCL = hito.montoCL
    logHito.montoUF = hito.montoUF # Monto con el que viene el hito
    logHito.montoDivisa = hito.montoDivisa
    logHito.divisa = hito.divisa
    logHito.tipoCambioDivisa = hito.tipoCambioDivisa
    logHito.tipoCambioUF = hito.tipoCambioUF
    logHito.montoFacturado = hito.montoFacturado
    logHito.notaVenta_id = hito.notaVenta_id_id
    logHito.save()
    return logHito

def ingresar_hito(request):
    """ Ingreso de Hito mediante el formulario, ademas de guardar esta accion en logHitos. """
    size_log = cant_notificaciones()     
    clientes = Cliente.objects.all()
    hitos = Hito.objects.all()
    if request.POST:
        try:
            hito = Hito()     
            notaVentaObjeto = NotaVenta(id=request.POST['notaVenta'])
            notaVenta = NotaVenta.objects.get(pk=request.POST['notaVenta'])
            hito.notaVenta_id = notaVentaObjeto   
            fecha = '-'.join(reversed(request.POST['fecha'].split('/')))
            montoUF = float(unicode(request.POST['montoUF']))
            tipoCambioUF = float(unicode(notaVenta.tipoCambioUF))        
            tipoCambioDivisa = float(unicode(notaVenta.tipoCambioDivisa))
            hito.tipoCambioUF = tipoCambioUF
            hito.tipoCambioDivisa = tipoCambioDivisa
            hito.montoUF = montoUF
            montoCL = tipoCambioUF * montoUF
            hito.montoCL = montoCL
            hito.montoDivisa = tipoCambioDivisa * montoCL
            hito.divisa = notaVenta.divisa
            hito.fecha = fecha
            hito.estado = request.POST['estado']
            hito.descripcion = request.POST['descripcion']
            hito.comentarios = request.POST['comentarios']
            hito.montoFacturado = 0
            hito.fechaEdicion = None
            hito.save()
            #Ingreso de Hito tambien en logHitos
            logHito = llenarLogHitos(hito.id)
            LogHitos.objects.filter(pk=logHito.id).update(tipo="Ingreso Hito",fechaEdicion=None,  montoUFEdicion=0)
            exito = "El Hito se ingreso correctamente."
            return render_to_response('hitos.html', {'exito': exito, 'size_log': size_log, 'clientes': clientes, 'hitos': hitos}, context_instance=RequestContext(request)) 
        except:
            return render_to_response('hitos.html', {'error': 'Ocurrio un error al ingresar el Hito, intentelo nuevamente.', 'size_log': size_log, 'clientes': clientes, 'hitos': hitos },context_instance=RequestContext(request))  

def buscar_hito(request):
    """ Busca el(los) Hito(s) a partir de los filtros indicados por el usuario en el buscador de Hito, si no 
    hay filtro se envian todas los Hitos. """
    size_log = cant_notificaciones()  
    clientes = Cliente.objects.all()
    hitos = Hito()
    hitoVacio = None
    if request.POST:
        cliente_id = request.POST.get('cliente' , False)
        negocio_id = request.POST.get('negocio', False)
        notaVenta_id = request.POST.get('notaVenta', False)
        estado = request.POST.get('estado', False)
        if request.POST.get('fechaHitoDesde',False) and request.POST.get('fechaHitoHasta',False):
            fechaHitoDesde = '-'.join(reversed(request.POST['fechaHitoDesde'].split('/')))
            fechaHitoHasta = '-'.join(reversed(request.POST['fechaHitoHasta'].split('/')))
        else:
            fechaHitoDesde = None
            fechaHitoHasta = None

        #Todos los hitos con Nota de Venta = e & estado = c & Fecha = Delta d (11)        
        if notaVenta_id and estado and fechaHitoDesde and fechaHitoHasta:
            hitos = Hito.objects.filter(notaVenta_id_id__exact=notaVenta_id, estado =estado, fecha__range=[fechaHitoDesde, fechaHitoHasta])
        
        #Todos los hitos del negocio = b & estado = c & Fecha = Delta d (8)            
        elif negocio_id and estado and fechaHitoDesde and fechaHitoHasta:
            notaVentas = NotaVenta.objects.filter(negocio_id_id__exact=negocio_id)
            for notaVenta in notaVentas:
                hitosxNotaVenta = Hito.objects.filter(notaVenta_id_id__exact=notaVenta.id, estado =estado, fecha__range=[fechaHitoDesde, fechaHitoHasta])
                hitos.append(hitosxNotaVenta) 

        #Todos los hitos del cliente = a & estado = c & Fecha = Delta d (4)
        elif cliente_id and estado and fechaHitoDesde and fechaHitoHasta:
            negocios = Negocio.objects.filter(cliente_id_id__exact=cliente_id)
            for negocio in negocios:
                notaVentas = NotaVenta.objects.filter(negocio_id_id__exact=negocio.id)
                for notaVenta in notaVentas:
                    hitosxNotaVenta = Hito.objects.filter(notaVenta_id_id__exact=notaVenta.id, estado =estado, fecha__range=[fechaHitoDesde, fechaHitoHasta])
                    hitos.append(hitosxNotaVenta)

        #Todos los hitos con Nota de Venta = e & Fecha = Delta d (12)
        elif fechaHitoDesde and fechaHitoHasta and notaVenta_id:
            hitos = Hito.objects.filter(notaVenta_id_id__exact=notaVenta_id, fecha__range=[fechaHitoDesde, fechaHitoHasta])
        
        #Todos los hitos con Nota de Venta =  e y estado = c (10)
        elif notaVenta_id and estado:
            hitos = Hito.objects.filter(notaVenta_id_id__exact=notaVenta_id, estado =estado)        
        
        #Todos los hitos con Negocio = b & Fecha = Delta d (9)
        elif fechaHitoDesde and fechaHitoHasta and negocio_id:
            notaVentas = NotaVenta.objects.filter(negocio_id_id__exact=negocio_id)
            for notaVenta in notaVentas:
                hitosxNotaVenta= Hito.objects.filter(notaVenta_id_id__exact=notaVenta.id, fecha__range=[fechaHitoDesde, fechaHitoHasta]) 
                hitos.append(hitosxNotaVenta)

        #Todos los hitos del negocio = b & estado = c (7)            
        elif negocio_id and estado:
            notaVentas = NotaVenta.objects.filter(negocio_id_id__exact=negocio_id)
            for notaVenta in notaVentas:
                hitosxNotaVenta = Hito.objects.filter(notaVenta_id_id__exact=notaVenta.id, estado =estado)
                hitos.append(hitosxNotaVenta)

        #Todos los hitos con cliente = a & estado = c (3)
        elif cliente_id and estado:
            negocios = Negocio.objects.filter(cliente_id_id__exact=cliente_id)
            for negocio in negocios:
                notaVentas = NotaVenta.objects.filter(negocio_id_id__exact=negocio.id)
                for notaVenta in notaVentas:
                    hitosxNotaVenta = Hito.objects.filter(notaVenta_id_id__exact=notaVenta.id, estado =estado)
                    hitos.append(hitosxNotaVenta)

        #Todos los hitos con cliente = a & Fecha = Delta d (5)
        elif cliente_id and fechaHitoDesde and fechaHitoHasta:
            negocios = Negocio.objects.filter(cliente_id_id__exact=cliente_id)
            for negocio in negocios:
                notaVentas = NotaVenta.objects.filter(negocio_id_id__exact=negocio.id)
                for notaVenta in notaVentas:
                    hitosxNotaVenta = Hito.objects.filter(notaVenta_id_id__exact=notaVenta.id, fecha__range=[fechaHitoDesde, fechaHitoHasta])
                    hitos.append(hitosxNotaVenta)

        #Todos los hitos durante Fecha = Delta d (14)
        elif fechaHitoDesde and fechaHitoHasta:
            hitos = Hito.objects.filter(fecha__range=[fechaHitoDesde, fechaHitoHasta])

        #Todos los hitos con Nota de Venta =  e (6)
        elif notaVenta_id:
            hitos = Hito.objects.filter(notaVenta_id_id__exact=notaVenta_id)

        #Todos los hitos del cliente = a (1)   
        elif cliente_id:
            hitos = []
            negocios = Negocio.objects.filter(cliente_id_id__exact=cliente_id)
            for negocio in negocios:
                notaVentas = NotaVenta.objects.filter(negocio_id_id__exact=negocio.id)
                for notaVenta in notaVentas:
                    hitosxNotaVenta= Hito.objects.filter(notaVenta_id_id__exact=notaVenta.id)
                    hitos.append(hitosxNotaVenta)
        #Todos los hitos del negocio =  b (2)            
        elif negocio_id:
            notaVentas = NotaVenta.objects.filter(negocio_id_id__exact=negocio_id)
            for notaVenta in notaVentas:
                hitosxNotaVenta = Hito.objects.filter(notaVenta_id_id__exact=notaVenta.id)
                hitos.append(hitosxNotaVenta)
        #Todos los hitos con estado = c (13)        
        elif estado:        
            hitos = Hito.objects.filter(estado=estado)
        elif fechaHitoDesde:
            hitos = Hito.objects.filter(fecha__gte=fechaHitoDesde)
        elif fechaHitoHasta:
            hitos = Hito.objects.filter(fecha__lte=fechaHitoHasta)                 
        else:
            hitos = Hito.objects.all()   
        if not hitos:
            hitoVacio = "No se encontraron Hitos bajo este filtro"   
    return render_to_response('hitos.html', {'hitoVacio': hitoVacio, 'size_log': size_log, 'hitos': hitos, 'clientes': clientes},context_instance=RequestContext(request))      

def agregarNV(request):
    """ Funcion que tiene asociada la vista agregarNV.html donde se ingresara una nueva Nota de Venta. """    
    size_log = cant_notificaciones() 
    clientes = Cliente.objects.all()
    ejecutivos = EjecutivoComercial.objects.all()
    return render_to_response('agregarNV.html',  {'size_log': size_log, 'clientes': clientes, 'ejecutivos': ejecutivos} , context_instance=RequestContext(request))

def agregarHito(request):
    """ Funcion que tiene asociada la vista agregarHito.html donde se ingresara un nuevo Hito. """
    size_log = cant_notificaciones() 
    clientes = Cliente.objects.all()
    return render_to_response('agregarHito.html',  {'size_log': size_log, 'clientes': clientes } , context_instance=RequestContext(request))

def editar_hito(request):
    """ Funcion que edita el Hito a partir de los datos ingresados por el usuario en el formulario, se hace distincion 
    si se edita el MontoUF y la Fecha para enviar notificaciones. """
    size_log = cant_notificaciones() 
    #Crear el logHitos con hito_id (pueden existir muchos logHitos del mismo hito)
    clientes = Cliente.objects.all()    
    if request.POST:
        try:
            fecha = '-'.join(reversed(request.POST['fechaHitoEditar'].split('/')))   
            fecha_date = datetime.strptime(fecha, '%Y-%m-%d').date()   
            hito = Hito.objects.get(pk=request.POST['hito_id'])
            logHitoCount = LogHitos.objects.filter( Q(tipo__contains="Cambio de Fecha") | Q(tipo__contains="Cambio de Monto"), hito_id=request.POST['hito_id'], estado=False).count()            
            if (int(request.POST['montoHitoEditar']) != int(hito.montoUF)) and (fecha_date != hito.fecha):
                if logHitoCount >= 1:
                    return render_to_response('hitos.html',{'error': "Este hito esta esperando aprobacion, no puedes modificar ni el monto ni la fecha", 'size_log': size_log, 'clientes': clientes, 'hitos': None }, context_instance=RequestContext(request))    

                logHitoFecha = llenarLogHitos(hito.id)
                LogHitos.objects.filter(pk=logHitoFecha.id).update(tipo="Cambio de Fecha",fechaEdicion=fecha_date,  montoUFEdicion=0)
                logHitoMonto = llenarLogHitos(hito.id)
                LogHitos.objects.filter(pk=logHitoMonto.id).update(tipo="Cambio de Monto", montoUFEdicion=request.POST['montoHitoEditar'], fechaEdicion =None)
                Hito.objects.filter(pk=request.POST['hito_id']).update(fechaEdicion=fecha_date, montoUFEdicion=request.POST['montoHitoEditar'])    

            elif int(request.POST['montoHitoEditar']) != int(hito.montoUF) : 
                if logHitoCount >= 1:
                    return render_to_response('hitos.html',{'error': "Este hito esta esperando aprobacion, no puedes modificar ni el monto ni la fecha", 'size_log': size_log, 'clientes': clientes, 'hitos': None }, context_instance=RequestContext(request))    

                logHito = llenarLogHitos(hito.id)
                LogHitos.objects.filter(pk=logHito.id).update(tipo="Cambio de Monto", montoUFEdicion=request.POST['montoHitoEditar'], fechaEdicion=None)            
                Hito.objects.filter(pk=request.POST['hito_id']).update(montoUFEdicion=request.POST['montoHitoEditar']) 

            elif fecha_date != hito.fecha:
                if logHitoCount >= 1:
                    return render_to_response('hitos.html',{'error': "Este hito esta esperando aprobacion, no puedes modificar ni el monto ni la fecha", 'size_log': size_log, 'clientes': clientes, 'hitos': None }, context_instance=RequestContext(request))    
             
                logHito = llenarLogHitos(hito.id)
                LogHitos.objects.filter(pk=logHito.id).update(tipo="Cambio de Fecha",fechaEdicion=fecha_date, montoUFEdicion=0)
                Hito.objects.filter(pk=request.POST['hito_id']).update(fechaEdicion=fecha)        

            if request.POST['estadoHitoEditar'] != hito.estado or request.POST['descripcionHitoEditar'] != hito.descripcion or request.POST['comentariosHitoEditar'] != hito.comentarios:

                Hito.objects.filter(pk=request.POST['hito_id']).update(estado=request.POST['estadoHitoEditar'],
                descripcion = request.POST ['descripcionHitoEditar'], comentarios = request.POST ['comentariosHitoEditar'])
                log = llenarLogHitos(hito.id)
                LogHitos.objects.filter(pk=log.id).update(tipo="Edicion Hito",fechaEdicion=None, montoUFEdicion=0)
            exito = "El Hito fue editado exitosamente."
            return render_to_response('hitos.html',{'exito': exito, 'size_log': size_log, 'clientes': clientes, 'hitos': None }, context_instance=RequestContext(request))    
        except:
            return render_to_response('hitos.html',{'error': 'Ocurrio un error al editar el Hito, intentelo nuevamente.', 'size_log': size_log, 'clientes': clientes, 'hitos': None }, context_instance=RequestContext(request))    

def enviar_id(request):
    """ A partir del id del Hito se envia el objeto Hito asoaciado. """
    hitoId = request.POST.get('idComparacion', None)
    h = Hito.objects.get(pk=hitoId)
    datajson = serializers.serialize('json', [h])
    response = HttpResponse(datajson,
        content_type='application/json'
    )
    return response    

def modificar_fecha(request):
    """ Se modifica la fecha a partir de lo ingresado por el usuario, se guarda esta accion en logHitos para luego enviar
    notificacion. """
    size_log = cant_notificaciones() 
    clientes = Cliente.objects.all()   
    logHitoCount = LogHitos.objects.filter(hito_id__exact=request.POST['hitoModFecha_id'], estado=False).count()            
    if request.POST:
        try:
            motivoModFecha = request.POST['motivoModFecha']
            fechaEdicion = '-'.join(reversed(request.POST['fechaHitoModFecha'].split('/')))
            hitoObjeto = Hito(id=request.POST['hitoModFecha_id'])
            hito = Hito.objects.get(pk=request.POST['hitoModFecha_id'])
            fecha_date = datetime.strptime(fechaEdicion, '%Y-%m-%d').date() 
            if fecha_date != hito.fecha:
                if logHitoCount >= 1:
                    return render_to_response('hitos.html',{'error': "Este hito esta esperando aprobacion, no puedes modificar ni el monto ni la fecha", 'size_log': size_log, 'clientes': clientes, 'hitos': None }, context_instance=RequestContext(request))                
                logHito = llenarLogHitos(hito.id)
                LogHitos.objects.filter(pk=logHito.id).update(tipo="Cambio de Fecha",fechaEdicion=fecha_date, montoUFEdicion=0)
                Hito.objects.filter(pk=hito.id).update(fechaEdicion=fecha_date)        
                # Hito.objects.filter(pk=request.POST['hitoModFecha_id']).update(fechaEdicion=fechaEdicion)
                exito = "Se ha modificado la fecha exitosamente."
                return render_to_response('hitos.html',{'exito': exito, 'size_log': size_log, 'clientes':clientes, 'hitos': None}, context_instance=RequestContext(request))
            else:
                return render_to_response('hitos.html',{'error': 'No has editado la fecha.', 'size_log': size_log, 'clientes':clientes, 'hitos': None}, context_instance=RequestContext(request))
        except:
            return render_to_response('hitos.html',{'error': 'Ocurrio un error al modificar la fecha, intentelo nuevamente.' , 'size_log': size_log, 'clientes':clientes, 'hitos': None}, context_instance=RequestContext(request))

def eliminar_hito(request):
    """ Funcion que elimina el Hito, guardando ademas esta accion en logHitos. """
    size_log = cant_notificaciones() 
    clientes = Cliente.objects.all()    
    if request.POST:
        try:
            hito = request.POST.get('idHitoAEliminar', False)
            logHito = llenarLogHitos(hito)
            LogHitos.objects.filter(pk=logHito.id).update(tipo="Eliminacion Hito",fechaEdicion=None,  montoUFEdicion=0)
            Hito.objects.get(pk=hito).delete()
            exito = "El hito ha sido eliminado exitosamente."
            return render_to_response('hitos.html', {'exito': exito, 'size_log': size_log, 'clientes' : clientes}, context_instance=RequestContext(request))                
        except:
            return render_to_response('hitos.html', {'error': "Ocurrio un error al eliminar el Hito, intentelo nuevamente.", 'size_log': size_log, 'clientes' : clientes}, context_instance=RequestContext(request))                
                    
def buscar_balance_fecha(request):
    """ Se buscan los hitos asociados segun la fecha ingresada por el usuario en el buscador de balance. """
    size_log = cant_notificaciones() 
    clientes = Cliente.objects.all()
    lista_balance = []
    lista_balance_mes = []    
    monto_acumulado = 0   
    if request.POST:
        mesDesde = request.POST.get('fechaMesDesde' , False)
        mesHasta = request.POST.get('fechaMesHasta', False)
        rangoAnnio = request.POST.get('rangoAnnio', False)        
        fechaMes = request.POST.get('fechaMes', False)
        fechaAnnio = request.POST.get('fechaAnnio', False)
        #Con mes de inicio, mes final y año       
        if mesDesde and mesHasta and rangoAnnio:
            fechaDesde = datetime(int(rangoAnnio),int(mesDesde), 01)
            ult_dia = calendar.monthrange(int(rangoAnnio), int(mesHasta))[1]
            fechaHasta = datetime(int(rangoAnnio),int(mesHasta), ult_dia)
            hitos = Hito.objects.filter(fecha__range=[fechaDesde, fechaHasta]).order_by('fecha')
            #devuelvo lista con cliente, NV y negocio asociado
            lista_balance = devolver_lista_balance(hitos)    
        elif fechaMes:
            fechaDesde = datetime(date.today().year,int(fechaMes), 01)
            ult_dia = calendar.monthrange(date.today().year, int(fechaMes))[1]          
            fechaHasta = datetime(2014,int(fechaMes), ult_dia)
            hitos = Hito.objects.filter(fecha__range=[fechaDesde, fechaHasta]).order_by('fecha')
            lista_balance_mes, monto_acumulado = devolver_lista(hitos)
        elif fechaAnnio:
            fechaDesde = datetime(int(fechaAnnio),01, 01)
            fechaHasta = datetime(int(fechaAnnio),12, 31)
            hitos = Hito.objects.filter(fecha__range=[fechaDesde, fechaHasta]).order_by('fecha')
            #devuelvo lista con cliente, NV y negocio asociado
            lista_balance = devolver_lista_balance(hitos)               
        else:
            fechaDesde = datetime( date.today().year,01, 01)
            fechaHasta = datetime( date.today().year,12, 31)
            hitos = Hito.objects.filter(fecha__range=[fechaDesde, fechaHasta]).order_by('fecha')
            #Por defecto devuelvo lista con cliente, NV y negocio asociado del año
            lista_balance = devolver_lista_balance(hitos)  
        if lista_balance:
            return render_to_response('balance.html', {'size_log': size_log, 'monto_acumulado': monto_acumulado, 'lista_balance': lista_balance, 'clientes': clientes},context_instance=RequestContext(request))      
        elif lista_balance_mes: 
            return render_to_response('balance.html', {'size_log': size_log, 'monto_acumulado': monto_acumulado, 'lista_balance_mes': lista_balance_mes, 'clientes': clientes},context_instance=RequestContext(request))      
        else:      
            return render_to_response('balance.html', {'error': 'No se encontraron hitos bajo este filtro.', 'size_log': size_log, 'monto_acumulado': monto_acumulado, 'clientes': clientes},context_instance=RequestContext(request))      
        
def buscar_balance_cliente(request):
    """ Se buscan los hitos asociados segun el cliente seleccionado por el usuario en el buscador de balance. """
    size_log = cant_notificaciones() 
    hitos = []
    monto_acumulado = 0
    clientes = Cliente.objects.all()
    if request.POST:
        cliente_id = request.POST.get('cliente_id' , False)
        if not cliente_id:
            hitos = None
            return render_to_response('balance.html', {'error': 'Seleccione algun Cliente para realizar la busqueda.', 'size_log': size_log, 'monto_acumulado': monto_acumulado, 'clientes': clientes},context_instance=RequestContext(request))      
        else:           
            cliente = Cliente.objects.get(pk=cliente_id) 
            negocios = Negocio.objects.filter(cliente_id_id__exact=cliente)
            for negocio in negocios:
                notaVentas = NotaVenta.objects.filter(negocio_id_id__exact=negocio.id)
                for notaVenta in notaVentas:
                    hitosxCliente= Hito.objects.filter(notaVenta_id_id__exact=notaVenta.id)
                    for h in hitosxCliente:
                        monto_acumulado = monto_acumulado + h.montoUF
                        # hitos.append(hitosxNotaVenta)
                        hitos.append([h.id,h.fecha, h.estado, h.montoUF, h.descripcion, notaVenta.numeroNotaVenta, negocio.nombreNegocio,cliente.nombreFantasia])
            if not hitos:
                return render_to_response('balance.html', {'error': 'No se encontraron hitos bajo este filtro.', 'size_log': size_log, 'monto_acumulado': monto_acumulado, 'clientes': clientes},context_instance=RequestContext(request))      
    return render_to_response('balance.html', { 'size_log': size_log, 'hitos': hitos, 'monto_acumulado': monto_acumulado,'clientes': clientes},context_instance=RequestContext(request))      

def buscar_balance_negocio(request):
    """ Se buscan los hitos asoaciados segun el negocio ingresado por el usuario en el buscador de balance, ademas se
    genera un excel. """
    size_log = cant_notificaciones() 
    hitos = []
    monto_acumulado = 0
    clientes = Cliente.objects.all()
    libro = xlwt.Workbook(encoding = 'utf-8')
    hoja = libro.add_sheet('Balance Por Negocio', cell_overwrite_ok=True)
    if request.POST:
        cliente_id = request.POST.get('cliente' , False)
        negocio_id = request.POST.get('negocio', False) 
        if not negocio_id:
            hitos = None
            return render_to_response('balance.html', {'error': 'Seleccione algun Negocio para realizar la busqueda.', 'size_log': size_log, 'monto_acumulado': monto_acumulado, 'clientes': clientes},context_instance=RequestContext(request))      
        else:           
            cliente = Cliente.objects.get(pk=cliente_id) 
            if Negocio.objects.filter(pk=negocio_id).exists:
                negocio = Negocio.objects.get(pk=negocio_id)
                if NotaVenta.objects.filter(negocio_id_id__exact=negocio_id).count >= 1:
                    notaVentas = NotaVenta.objects.filter(negocio_id_id__exact=negocio_id)
                    for notaVenta in notaVentas:
                        hitosxCliente= Hito.objects.filter(notaVenta_id_id__exact=notaVenta.id)
                        for h in hitosxCliente:
                            monto_acumulado = monto_acumulado + h.montoUF
                            hitos.append([h.id,h.fecha, h.estado, h.montoUF, h.descripcion, notaVenta.numeroNotaVenta, negocio.nombreNegocio,cliente.nombreFantasia])
                        row = 4
                        for hi in hitos:
                            hoja.write(row, 1, hi[0])
                            hoja.write(row, 2, hi[1])
                            hoja.write(row, 3, hi[2])
                            hoja.write(row, 4, hi[3])
                            hoja.write(row, 5, hi[4])
                            hoja.write(row, 6, hi[5])
                            hoja.write(row, 7, hi[6])
                            hoja.write(row, 8, hi[7])
                            row = row+1
                        libro.save('Balance.xls')    
                else: hitos = None    
            else:
                hitos = None    
            if not hitos:
                return render_to_response('balance.html', {'error': 'No se encontraron hitos bajo este filtro.', 'size_log': size_log, 'monto_acumulado': monto_acumulado, 'clientes': clientes},context_instance=RequestContext(request))      
    return render_to_response('balance.html', {'size_log': size_log, 'hitos': hitos, 'monto_acumulado': monto_acumulado,'clientes': clientes},context_instance=RequestContext(request))      

def buscar_balance_NV(request):
    """ Se buscan los hitos asoaciados segun la Nota de Venta seleccionada por el usuario en el buscador de balance. """
    size_log = cant_notificaciones() 
    hitos = []
    monto_acumulado = 0
    clientes = Cliente.objects.all()
    if request.POST:
        cliente_id = request.POST.get('cliente_NV' , False)
        negocio_id = request.POST.get('negocio_NV', False)        
        NV_id = request.POST.get('notaVenta_NV', False)    
        if not NV_id:
            hitos = None
            return render_to_response('balance.html', {'error': 'Seleccione alguna Nota de Venta para realizar la busqueda.', 'size_log': size_log, 'monto_acumulado': monto_acumulado, 'clientes': clientes},context_instance=RequestContext(request))      
        else:             
            cliente = Cliente.objects.get(pk=cliente_id) 
            negocio = Negocio.objects.get(pk=negocio_id)
            notaVenta = NotaVenta.objects.get(pk=NV_id)
            hitosxCliente= Hito.objects.filter(notaVenta_id_id__exact=notaVenta.id)
            for h in hitosxCliente:
                monto_acumulado = monto_acumulado + h.montoUF
                hitos.append([h.id,h.fecha, h.estado, h.montoUF, h.descripcion, notaVenta.numeroNotaVenta, negocio.nombreNegocio,cliente.nombreFantasia])
            if not hitos:
                return render_to_response('balance.html', {'error': 'No se encontraron hitos bajo este filtro.', 'size_log': size_log, 'monto_acumulado': monto_acumulado, 'clientes': clientes},context_instance=RequestContext(request))         
    return render_to_response('balance.html', {'size_log': size_log, 'hitos': hitos, 'monto_acumulado': monto_acumulado,'clientes': clientes},context_instance=RequestContext(request))      

def aceptar_solicitud(request):
    """ Se acepta la solicitud de modicacion de fecha o montoUF. """
    lista = []
    if request.POST:
        try:
            logHito_id = request.POST.get('logHito_id',None)
            logHito = LogHitos.objects.get(pk=logHito_id)
            hito = Hito.objects.get(pk=logHito.hito_id)        
            if logHito.tipo == 'Cambio de Fecha': 
                Hito.objects.filter(pk=hito.id).update(fecha=hito.fechaEdicion, fechaEdicion = None)
            elif logHito.tipo == 'Cambio de Monto':   
                Hito.objects.filter(pk=hito.id).update(montoUF=hito.montoUFEdicion, montoUFEdicion=0)
            LogHitos.objects.filter(pk=logHito.id).update(estado=True)
            if LogHitos.objects.filter((Q(tipo__contains="Cambio de Fecha") | Q(tipo__contains="Cambio de Monto")), estado=False).exists():
                logHitos = LogHitos.objects.filter((Q(tipo__contains="Cambio de Fecha") | Q(tipo__contains="Cambio de Monto")),estado=False).order_by('fecha')
                for logHito in logHitos:
                    hito = Hito.objects.filter(pk=logHito.hito_id)
                    notaVenta = NotaVenta.objects.filter(pk=hito[0].notaVenta_id_id)
                    negocio = Negocio.objects.filter(pk=notaVenta[0].negocio_id_id)
                    cliente = Cliente.objects.filter(pk=negocio[0].cliente_id_id) 
                    lista.append([logHito.fecha, cliente[0].nombreFantasia, negocio[0].nombreNegocio, notaVenta[0].numeroNotaVenta,hito[0].id,logHito.tipo, hito[0].fecha, hito[0].fechaEdicion, hito[0].montoUF, hito[0].montoUFEdicion, logHito.id])
            else:
                lista = None
            exito = "Se realizo el cambio exitosamente"    
            return render_to_response('notificaciones.html',  {'exito': exito, 'lista': lista } , context_instance=RequestContext(request))
        except:
            return render_to_response('notificaciones.html',  {'error': "Ocurrio un error al aceptar la solicitud, intentelo nuevamente", 'lista': lista } , context_instance=RequestContext(request))

def eliminar_solicitud(request):
    """ Se elimina la solicitud de modificar la fecha o montoUF. """
    lista = []
    if request.POST:
        try:
            logHito_id = request.POST.get('logHito_eliminar',None)
            logHito = LogHitos.objects.get(pk=logHito_id)
            hito = Hito.objects.get(pk=logHito.hito_id)        
            if logHito.tipo == 'Cambio de Fecha': 
                Hito.objects.filter(pk=hito.id).update(fechaEdicion=None)
            elif logHito.tipo == 'Cambio de Monto':   
                Hito.objects.filter(pk=hito.id).update(montoUF=0)
            LogHitos.objects.filter(pk=logHito.id).update(estado=True)
            if LogHitos.objects.filter((Q(tipo__contains="Cambio de Fecha") | Q(tipo__contains="Cambio de Monto")), estado=False).exists():
                logHitos = LogHitos.objects.filter((Q(tipo__contains="Cambio de Fecha") | Q(tipo__contains="Cambio de Monto")),estado=False).order_by('fecha')
                for logHito in logHitos:
                    hito = Hito.objects.filter(pk=logHito.hito_id)
                    notaVenta = NotaVenta.objects.filter(pk=hito[0].notaVenta_id_id)
                    negocio = Negocio.objects.filter(pk=notaVenta[0].negocio_id_id)
                    cliente = Cliente.objects.filter(pk=negocio[0].cliente_id_id) 
                    lista.append([logHito.fecha, cliente[0].nombreFantasia, negocio[0].nombreNegocio, notaVenta[0].numeroNotaVenta,hito[0].id,logHito.tipo, hito[0].fecha, hito[0].fechaEdicion, hito[0].montoUF, hito[0].montoUFEdicion])
            else:
                lista = None
            exito = "Se elimino la solicitud exitosamente"    
            return render_to_response('notificaciones.html',  {'exito': exito, 'lista': lista } , context_instance=RequestContext(request))
        except:
            return render_to_response('notificaciones.html',  {'error': "Ocurrio un error al eliminar la solicitud, intentelo nuevamente", 'lista': lista } , context_instance=RequestContext(request))

def facturar_hito(request):
    """ Se factura el Hito asociando el documento tributario. """
    clientes = Cliente.objects.all()
    size_log = cant_notificaciones()        
    if request.POST:
        try:
            hito_id = request.POST.get('hito_id_facturar',None)
            montoUF = float(unicode(request.POST.get('montoHitoFacturar',None)))
            hitoObjeto = Hito(id=hito_id)
            hito = Hito.objects.get(pk=hito_id)
            # Si el hito no esta facturado completamente, se puede generar un documento tributario
            if (int(hito.montoUF - hito.montoFacturado)) >= (int(montoUF)) :
                docTributario = DocumentoTributario()
                docTributario.numeroDoc = request.POST.get('numeroDoc', None)
                docTributario.tipoDoc = request.POST.get('tipoDoc',None)
                docTributario.signo = request.POST.get('signo',None)
                docTributario.fecha = date.today()
                docTributario.observaciones = request.POST.get('observaciones',None)
                docTributario.totalUF = montoUF
                docTributario.totalCL = montoUF * hito.tipoCambioUF
                montoFacturado = montoUF + hito.montoFacturado
                Hito.objects.filter(pk=hito.id).update(montoFacturado = montoFacturado)
                docTributario.save()
                id_doc = docTributario.id
                DocumentoTributario.objects.filter(pk=id_doc).update(numeroDoc=id_doc)
                docTributarioHito = DocumentoTributarioHito()
                docTributarioHito.hito_id = hitoObjeto
                docTributarioHito.documentoTributario_id = docTributario
                docTributarioHito.save()
                if int(montoFacturado) == int(hito.montoUF):
                    #El hito ya se facturo completamente, cambia de estado a facturado                
                    Hito.objects.filter(pk=hito.id).update(estado= 5)
                exito= "El hito se facturo exitosamente."    
                return render_to_response('hitos.html',{'exito': exito, 'size_log': size_log, 'clientes': clientes , 'hitos': None }, context_instance=RequestContext(request))    
            else:        
                return render_to_response('hitos.html',{'error': 'Ocurrio un error ya que el monto ingresado supera al monto del hito. ', 'size_log': size_log, 'clientes': clientes , 'hitos': None }, context_instance=RequestContext(request))     
        except:
                return render_to_response('hitos.html',{'error': 'Ocurrio un error al facturar el Hito, intentelo nuevamente.', 'size_log': size_log, 'clientes': clientes , 'hitos': None }, context_instance=RequestContext(request))    
                
def verificarMontoPorNV(request):   
    """ Funcion que verifica que el monto ingresado en hitos por el usuario no supere el total por Nota de Venta. """             
    error = ''
    success = False
    if request.method == 'POST':
        notaVenta = request.POST.get('notaVenta', None)
        montoUF = request.POST.get('montoUF', None)
        datos = notaVenta.split('=')
        dat = datos[1]
        valores = montoUF.split('=')
        montoIngresado = valores[1]
        mensaje = ''
        if NotaVenta.objects.filter(pk=dat).exists():
            notaVenta = NotaVenta.objects.get(pk=dat)
            montoNV = notaVenta.montoUF
            if Hito.objects.filter(notaVenta_id_id__exact=notaVenta.id).exists():
                hitos = Hito.objects.filter(notaVenta_id_id__exact=notaVenta.id)
                montoPorHitos = 0
                for hito in hitos:
                    montoPorHitos = int(hito.montoUF) + int(montoPorHitos)
                if (montoPorHitos + int(montoIngresado)) <= int(montoNV):
                    mensaje = 'si'  
                else:             
                    mensaje = 'no'    
            else:
                if int(montoNV) >= int(montoIngresado):
                    mensaje = 'si'
                else:
                    mensaje = 'no'            
        else:
            mensaje = 'no'
    return HttpResponse(mensaje,mimetype='application/javascript')    

def buscar_logHito(request):
    """ Función que busca el log de Hitos mostrando solo los log asociados a la Nota de Venta seleccionada. """
    size_log = cant_notificaciones()    
    lista = []    
    logHitoVacio = None
    notaVentas = NotaVenta.objects.all()
    if request.POST:
        notaVenta_id = request.POST.get('notaVenta', None)
        if not notaVenta_id:
            lista = None
            logHitoVacio = "Seleccione alguna Nota de Venta para realizar la busqueda. "  
        else:          
            if LogHitos.objects.filter(notaVenta_id=notaVenta_id).exists():
                logHitos = LogHitos.objects.filter(notaVenta_id=notaVenta_id)
                notaVenta = NotaVenta.objects.filter(pk=notaVenta_id)
                for logHito in logHitos:    
                    negocio = Negocio.objects.filter(pk=notaVenta[0].negocio_id_id)
                    cliente = Cliente.objects.filter(pk=negocio[0].cliente_id_id) 
                    if logHito.estado == True:
                        estado = "Gestionado"
                    elif logHito.estado == False and (logHito.tipo == "Cambio de Fecha" or logHito.tipo == "Cambio de Monto"):
                        estado = "En espera"    
                    else:
                        estado= "No aplica"    
                    lista.append([logHito.fecha, cliente[0].nombreFantasia, negocio[0].nombreNegocio, notaVenta[0].numeroNotaVenta,logHito.hito_id,logHito.tipo, logHito.fechaHito, logHito.fechaEdicion, logHito.montoUF, logHito.montoUFEdicion, logHito.id, estado])
            else:
                lista = None    
                logHitoVacio = "No se encontraron log de hitos para esta Nota de Venta. "        
        return render_to_response('logHitos.html',{'logHitoVacio': logHitoVacio, 'notaVentas': notaVentas, 'size_log': size_log, 'lista': lista}, context_instance=RequestContext(request) )
