from django.db import models
from apps.models import Cliente, Hito, Negocio, NotaVenta
from django.forms import ModelForm
from django import forms

 
class ClienteForm(ModelForm):
    class Meta:
        model = Cliente
        fields = ['rut', 'razonSocial', 'nombreFantasia', 'giro', 'grupoEconomico' , 'sector', 'telefono', 'fax', 'direccion',  'comuna', 'ciudad', 'idCuentaZoho', 'region']
        #fields = ['nombreCliente', 'rut', 'razonSocial', 'nombreFantasia', 'giro', 'grupoEconomico', 'sector', 'telefono', 'fax', 'direccion', 'region', 'ciudad', 'comuna', 'idCuentaZoho']
class NegocioForm(ModelForm):
	class Meta:
		model = Negocio
		fields = ['cliente_id', 'nombreNegocio','descripcion', 'idejecutivoComercial','fechaNegocio','idOportunidadZoho']
		#fields = ['nombreNegocio','descripcion', 'idejecutivoComercial','fechaNegocio','idOportunidadZoho']

class NotaVentaForm(ModelForm):
	class Meta:
		model = NotaVenta 
		fields = ['numeroNotaVenta', 'fechaNotaVenta','montoUF','monedaOriginal','tipoCambioDivisa','tipoCambioUF','divisa','instFacturacion','tipoServicio','subTipoServicio','nombreServicio','glosa','margenNegocioPercent','margenNegocioUF','responsableDelivery','correoRespDelivery','negocio_id']

class HitoForm(ModelForm):
	class Meta:
		model = Hito
		fields = ['estado', 'fecha', 'comentarios', 'descripcion','montoUF', 'montoFacturado']		 	

class MinegocioForm(forms.Form):
	nombreCliente = forms.CharField(max_length=45)			
	nombreNegocio = forms.CharField(max_length=45)
	descripcion = forms.CharField(max_length=100)
	fecha = forms.DateField()			
	ejecutivo = forms.CharField(max_length=45)		

