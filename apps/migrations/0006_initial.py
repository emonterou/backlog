# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Cliente'
        db.create_table(u'apps_cliente', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('rut', self.gf('django.db.models.fields.CharField')(unique=True, max_length=12)),
            ('razonSocial', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('nombreFantasia', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('grupoEconomico', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('giro', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('fax', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('direccion', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('comuna', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('ciudad', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('sector', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('idCuentaZoho', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('region', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('activo', self.gf('django.db.models.fields.BooleanField')()),
        ))
        db.send_create_signal(u'apps', ['Cliente'])

        # Adding model 'Negocio'
        db.create_table(u'apps_negocio', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombreNegocio', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('descripcion', self.gf('django.db.models.fields.TextField')()),
            ('idejecutivoComercial', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('fechaNegocio', self.gf('django.db.models.fields.DateField')()),
            ('idOportunidadZoho', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('cliente_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.Cliente'])),
        ))
        db.send_create_signal(u'apps', ['Negocio'])

        # Adding model 'NotaVenta'
        db.create_table(u'apps_notaventa', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('numeroNotaVenta', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('fechaNotaVenta', self.gf('django.db.models.fields.DateField')()),
            ('montoUF', self.gf('django.db.models.fields.FloatField')()),
            ('montoDivisa', self.gf('django.db.models.fields.FloatField')()),
            ('montoCL', self.gf('django.db.models.fields.FloatField')()),
            ('monedaOriginal', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('tipoCambioDivisa', self.gf('django.db.models.fields.FloatField')()),
            ('tipoCambioUF', self.gf('django.db.models.fields.FloatField')()),
            ('divisa', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('instFacturacion', self.gf('django.db.models.fields.TextField')()),
            ('tipoServicio', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('subTipoServicio', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('nombreServicio', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('glosa', self.gf('django.db.models.fields.TextField')()),
            ('margenNegocioPercent', self.gf('django.db.models.fields.FloatField')()),
            ('margenNegocioUF', self.gf('django.db.models.fields.FloatField')()),
            ('responsableDelivery', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('correoRespDelivery', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('linkNotaVenta', self.gf('django.db.models.fields.TextField')()),
            ('linkOC', self.gf('django.db.models.fields.TextField')()),
            ('linkPropuesta', self.gf('django.db.models.fields.TextField')()),
            ('linkEstimacion', self.gf('django.db.models.fields.TextField')()),
            ('negocio_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.Negocio'])),
            ('estado', self.gf('django.db.models.fields.CharField')(max_length=45)),
        ))
        db.send_create_signal(u'apps', ['NotaVenta'])

        # Adding model 'DocumentoTributario'
        db.create_table(u'apps_documentotributario', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('numeroDoc', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('tipoDoc', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('signo', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('totalCL', self.gf('django.db.models.fields.FloatField')()),
            ('totalUF', self.gf('django.db.models.fields.FloatField')()),
            ('observaciones', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'apps', ['DocumentoTributario'])

        # Adding model 'Hito'
        db.create_table(u'apps_hito', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('estado', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('comentarios', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('montoCL', self.gf('django.db.models.fields.FloatField')()),
            ('montoUF', self.gf('django.db.models.fields.FloatField')()),
            ('montoDivisa', self.gf('django.db.models.fields.FloatField')()),
            ('divisa', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('tipoCambioDivisa', self.gf('django.db.models.fields.FloatField')()),
            ('tipoCambioUF', self.gf('django.db.models.fields.FloatField')()),
            ('montoFacturado', self.gf('django.db.models.fields.FloatField')()),
            ('estadoCompromiso', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('fechaEdicion', self.gf('django.db.models.fields.DateTimeField')()),
            ('notaVenta_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.NotaVenta'])),
        ))
        db.send_create_signal(u'apps', ['Hito'])

        # Adding model 'DocumentoTributarioHito'
        db.create_table(u'apps_documentotributariohito', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('documentoTributario_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.DocumentoTributario'])),
            ('hito_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.Hito'])),
        ))
        db.send_create_signal(u'apps', ['DocumentoTributarioHito'])

        # Adding model 'LogHitos'
        db.create_table(u'apps_loghitos', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('hito_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.Hito'])),
            ('estado', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'apps', ['LogHitos'])

        # Adding model 'EjecutivoComercial'
        db.create_table(u'apps_ejecutivocomercial', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombres', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('apellidos', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=100)),
        ))
        db.send_create_signal(u'apps', ['EjecutivoComercial'])

        # Adding model 'Participacion'
        db.create_table(u'apps_participacion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('porcentaje', self.gf('django.db.models.fields.FloatField')()),
            ('notaVenta_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.NotaVenta'])),
            ('ejecutivoComercial_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.EjecutivoComercial'])),
        ))
        db.send_create_signal(u'apps', ['Participacion'])

        # Adding model 'BalanceAnual'
        db.create_table(u'apps_balanceanual', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('totalNegocioUF', self.gf('django.db.models.fields.FloatField')()),
            ('anho', self.gf('django.db.models.fields.IntegerField')()),
            ('backlogInicio', self.gf('django.db.models.fields.FloatField')()),
            ('totalAnualUF', self.gf('django.db.models.fields.FloatField')()),
            ('totalProxUF', self.gf('django.db.models.fields.FloatField')()),
            ('balanceAnt_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.BalanceAnual'])),
            ('notaVenta_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.NotaVenta'])),
        ))
        db.send_create_signal(u'apps', ['BalanceAnual'])


    def backwards(self, orm):
        # Deleting model 'Cliente'
        db.delete_table(u'apps_cliente')

        # Deleting model 'Negocio'
        db.delete_table(u'apps_negocio')

        # Deleting model 'NotaVenta'
        db.delete_table(u'apps_notaventa')

        # Deleting model 'DocumentoTributario'
        db.delete_table(u'apps_documentotributario')

        # Deleting model 'Hito'
        db.delete_table(u'apps_hito')

        # Deleting model 'DocumentoTributarioHito'
        db.delete_table(u'apps_documentotributariohito')

        # Deleting model 'LogHitos'
        db.delete_table(u'apps_loghitos')

        # Deleting model 'EjecutivoComercial'
        db.delete_table(u'apps_ejecutivocomercial')

        # Deleting model 'Participacion'
        db.delete_table(u'apps_participacion')

        # Deleting model 'BalanceAnual'
        db.delete_table(u'apps_balanceanual')


    models = {
        u'apps.balanceanual': {
            'Meta': {'object_name': 'BalanceAnual'},
            'anho': ('django.db.models.fields.IntegerField', [], {}),
            'backlogInicio': ('django.db.models.fields.FloatField', [], {}),
            'balanceAnt_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.BalanceAnual']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notaVenta_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.NotaVenta']"}),
            'totalAnualUF': ('django.db.models.fields.FloatField', [], {}),
            'totalNegocioUF': ('django.db.models.fields.FloatField', [], {}),
            'totalProxUF': ('django.db.models.fields.FloatField', [], {})
        },
        u'apps.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'activo': ('django.db.models.fields.BooleanField', [], {}),
            'ciudad': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'comuna': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'giro': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'grupoEconomico': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idCuentaZoho': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'nombreFantasia': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'razonSocial': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'region': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'rut': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '12'}),
            'sector': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'apps.documentotributario': {
            'Meta': {'object_name': 'DocumentoTributario'},
            'fecha': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numeroDoc': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'observaciones': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'signo': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'tipoDoc': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'totalCL': ('django.db.models.fields.FloatField', [], {}),
            'totalUF': ('django.db.models.fields.FloatField', [], {})
        },
        u'apps.documentotributariohito': {
            'Meta': {'object_name': 'DocumentoTributarioHito'},
            'documentoTributario_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.DocumentoTributario']"}),
            'hito_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Hito']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'apps.ejecutivocomercial': {
            'Meta': {'object_name': 'EjecutivoComercial'},
            'apellidos': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombres': ('django.db.models.fields.CharField', [], {'max_length': '45'})
        },
        u'apps.hito': {
            'Meta': {'object_name': 'Hito'},
            'comentarios': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'divisa': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'estadoCompromiso': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'fechaEdicion': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'montoCL': ('django.db.models.fields.FloatField', [], {}),
            'montoDivisa': ('django.db.models.fields.FloatField', [], {}),
            'montoFacturado': ('django.db.models.fields.FloatField', [], {}),
            'montoUF': ('django.db.models.fields.FloatField', [], {}),
            'notaVenta_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.NotaVenta']"}),
            'tipoCambioDivisa': ('django.db.models.fields.FloatField', [], {}),
            'tipoCambioUF': ('django.db.models.fields.FloatField', [], {})
        },
        u'apps.loghitos': {
            'Meta': {'object_name': 'LogHitos'},
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'hito_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Hito']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '45'})
        },
        u'apps.negocio': {
            'Meta': {'object_name': 'Negocio'},
            'cliente_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Cliente']"}),
            'descripcion': ('django.db.models.fields.TextField', [], {}),
            'fechaNegocio': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idOportunidadZoho': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'idejecutivoComercial': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'nombreNegocio': ('django.db.models.fields.CharField', [], {'max_length': '45'})
        },
        u'apps.notaventa': {
            'Meta': {'object_name': 'NotaVenta'},
            'correoRespDelivery': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'divisa': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'fechaNotaVenta': ('django.db.models.fields.DateField', [], {}),
            'glosa': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instFacturacion': ('django.db.models.fields.TextField', [], {}),
            'linkEstimacion': ('django.db.models.fields.TextField', [], {}),
            'linkNotaVenta': ('django.db.models.fields.TextField', [], {}),
            'linkOC': ('django.db.models.fields.TextField', [], {}),
            'linkPropuesta': ('django.db.models.fields.TextField', [], {}),
            'margenNegocioPercent': ('django.db.models.fields.FloatField', [], {}),
            'margenNegocioUF': ('django.db.models.fields.FloatField', [], {}),
            'monedaOriginal': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'montoCL': ('django.db.models.fields.FloatField', [], {}),
            'montoDivisa': ('django.db.models.fields.FloatField', [], {}),
            'montoUF': ('django.db.models.fields.FloatField', [], {}),
            'negocio_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Negocio']"}),
            'nombreServicio': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'numeroNotaVenta': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'responsableDelivery': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'subTipoServicio': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'tipoCambioDivisa': ('django.db.models.fields.FloatField', [], {}),
            'tipoCambioUF': ('django.db.models.fields.FloatField', [], {}),
            'tipoServicio': ('django.db.models.fields.CharField', [], {'max_length': '45'})
        },
        u'apps.participacion': {
            'Meta': {'object_name': 'Participacion'},
            'ejecutivoComercial_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.EjecutivoComercial']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notaVenta_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.NotaVenta']"}),
            'porcentaje': ('django.db.models.fields.FloatField', [], {})
        }
    }

    complete_apps = ['apps']