# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'LogHitos.hito_id'
        db.add_column(u'apps_loghitos', 'hito_id',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'LogHitos.hito_id'
        db.delete_column(u'apps_loghitos', 'hito_id')


    models = {
        u'apps.balanceanual': {
            'Meta': {'object_name': 'BalanceAnual'},
            'anho': ('django.db.models.fields.IntegerField', [], {}),
            'backlogInicio': ('django.db.models.fields.FloatField', [], {}),
            'balanceAnt_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.BalanceAnual']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notaVenta_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.NotaVenta']"}),
            'totalAnualUF': ('django.db.models.fields.FloatField', [], {}),
            'totalNegocioUF': ('django.db.models.fields.FloatField', [], {}),
            'totalProxUF': ('django.db.models.fields.FloatField', [], {})
        },
        u'apps.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'activo': ('django.db.models.fields.BooleanField', [], {}),
            'ciudad': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'comuna': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'giro': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'grupoEconomico': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idCuentaZoho': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'nombreFantasia': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'razonSocial': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'region': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'rut': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '12'}),
            'sector': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'apps.documentotributario': {
            'Meta': {'object_name': 'DocumentoTributario'},
            'fecha': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numeroDoc': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'observaciones': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'signo': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'tipoDoc': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'totalCL': ('django.db.models.fields.FloatField', [], {}),
            'totalUF': ('django.db.models.fields.FloatField', [], {})
        },
        u'apps.documentotributariohito': {
            'Meta': {'object_name': 'DocumentoTributarioHito'},
            'documentoTributario_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.DocumentoTributario']"}),
            'hito_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Hito']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'apps.ejecutivocomercial': {
            'Meta': {'object_name': 'EjecutivoComercial'},
            'apellidos': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombres': ('django.db.models.fields.CharField', [], {'max_length': '45'})
        },
        u'apps.hito': {
            'Meta': {'object_name': 'Hito'},
            'comentarios': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'divisa': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'estadoCompromiso': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'fechaEdicion': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'montoCL': ('django.db.models.fields.FloatField', [], {}),
            'montoDivisa': ('django.db.models.fields.FloatField', [], {}),
            'montoFacturado': ('django.db.models.fields.FloatField', [], {}),
            'montoUF': ('django.db.models.fields.FloatField', [], {}),
            'montoUFEdicion': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'notaVenta_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.NotaVenta']"}),
            'tipoCambioDivisa': ('django.db.models.fields.FloatField', [], {}),
            'tipoCambioUF': ('django.db.models.fields.FloatField', [], {})
        },
        u'apps.loghitos': {
            'Meta': {'object_name': 'LogHitos'},
            'comentarios': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'divisa': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'estadoCompromiso': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'estadoHito': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '20'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'fechaEdicion': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'fechaHito': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'hito_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'montoCL': ('django.db.models.fields.FloatField', [], {}),
            'montoDivisa': ('django.db.models.fields.FloatField', [], {}),
            'montoFacturado': ('django.db.models.fields.FloatField', [], {}),
            'montoUF': ('django.db.models.fields.FloatField', [], {}),
            'montoUFEdicion': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'notaVenta_id': ('django.db.models.fields.IntegerField', [], {}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'tipoCambioDivisa': ('django.db.models.fields.FloatField', [], {}),
            'tipoCambioUF': ('django.db.models.fields.FloatField', [], {})
        },
        u'apps.negocio': {
            'Meta': {'object_name': 'Negocio'},
            'cliente_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Cliente']"}),
            'descripcion': ('django.db.models.fields.TextField', [], {}),
            'fechaNegocio': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idOportunidadZoho': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'idejecutivoComercial': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'nombreNegocio': ('django.db.models.fields.CharField', [], {'max_length': '45'})
        },
        u'apps.notaventa': {
            'Meta': {'object_name': 'NotaVenta'},
            'correoRespDelivery': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'divisa': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'fechaNotaVenta': ('django.db.models.fields.DateField', [], {}),
            'glosa': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instFacturacion': ('django.db.models.fields.TextField', [], {}),
            'linkEstimacion': ('django.db.models.fields.TextField', [], {}),
            'linkNotaVenta': ('django.db.models.fields.TextField', [], {}),
            'linkOC': ('django.db.models.fields.TextField', [], {}),
            'linkPropuesta': ('django.db.models.fields.TextField', [], {}),
            'margenNegocioPercent': ('django.db.models.fields.FloatField', [], {}),
            'margenNegocioUF': ('django.db.models.fields.FloatField', [], {}),
            'monedaOriginal': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'montoCL': ('django.db.models.fields.FloatField', [], {}),
            'montoDivisa': ('django.db.models.fields.FloatField', [], {}),
            'montoUF': ('django.db.models.fields.FloatField', [], {}),
            'negocio_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Negocio']"}),
            'nombreServicio': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'numeroNotaVenta': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'responsableDelivery': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'subTipoServicio': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'tipoCambioDivisa': ('django.db.models.fields.FloatField', [], {}),
            'tipoCambioUF': ('django.db.models.fields.FloatField', [], {}),
            'tipoServicio': ('django.db.models.fields.CharField', [], {'max_length': '45'})
        },
        u'apps.participacion': {
            'Meta': {'object_name': 'Participacion'},
            'ejecutivoComercial_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.EjecutivoComercial']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notaVenta_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.NotaVenta']"}),
            'porcentaje': ('django.db.models.fields.FloatField', [], {})
        }
    }

    complete_apps = ['apps']