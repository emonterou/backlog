from __future__ import unicode_literals
from django.db import models
from django.forms import ModelForm

class Cliente(models.Model):
	""" Tabla Cliente contiene los atributos de los clientes, el campo activo indica si el cliente tiene negocios con 
	la empresa, una vez que se crea el cliente este campo es True. """	
	rut = models.CharField(max_length=12, unique=True)
	razonSocial = models.CharField(max_length=100 , verbose_name="Razon Social")
	nombreFantasia = models.CharField(max_length=100, verbose_name="Nombre de Fantasia")
	grupoEconomico = models.CharField(max_length=100, verbose_name="Grupo Economico")
	giro = models.CharField(max_length=100)
	telefono = models.CharField(max_length=20)
	fax = models.CharField(max_length=20)
	direccion = models.CharField(max_length=100)
	comuna = models.CharField(max_length=45)
	ciudad = models.CharField(max_length=45)
	sector = models.CharField(max_length=45)
	idCuentaZoho = models.CharField(max_length=45, verbose_name="Cuenta Zoho")
	region = models.CharField(max_length=45)
	activo = models.BooleanField()
	
	def tieneNegocios(self):
		return len(self.negocios.all()[:1]) == 1

class Negocio(models.Model):
	""" Tabla Negocio, contiene los campos de los negocios, referenciada a la tabla Cliente. """	
	nombreNegocio = models.CharField(max_length=45,verbose_name="Nombre de Negocio")
	descripcion = models.TextField()
	idejecutivoComercial = models.CharField(max_length=45, verbose_name="Ejecutivo Comercial")
	fechaNegocio = models.DateField(verbose_name="Fecha de Negocio")
	idOportunidadZoho = models.CharField(max_length=45 , verbose_name="Oportunidad Zoho")
	cliente_id = models.ForeignKey(Cliente)

class NotaVenta(models.Model):
	""" Tabla NotaVenta, esta tabla contiene los atributos pertenecientes a la Nota de Venta como por ejemplo sus montos, 
	sus fechas, estados, etc. Esta referenciada a la tabla Negocio. """
	numeroNotaVenta = models.CharField(max_length=45)
	fechaNotaVenta = models.DateField()
	montoUF = models.FloatField(verbose_name="Monto UF")
	montoDivisa = models.FloatField(verbose_name="Monto Divisa")
	montoCL = models.FloatField(verbose_name="Monto Pesos")
	monedaOriginal = models.CharField(max_length=45, verbose_name="Moneda Original")
	tipoCambioDivisa = models.FloatField(verbose_name="Tipo de cambio Divisa")
	tipoCambioUF = models.FloatField(verbose_name="Tipo de cambio UF")
	divisa = models.CharField(max_length=45)
	instFacturacion = models.TextField(verbose_name="Instancia de facturacion")
	tipoServicio = models.CharField(max_length=45, verbose_name="Tipo de servicio")
	subTipoServicio = models.CharField(max_length=45, verbose_name="Sub tipo dr servicio")
	nombreServicio = models.CharField(max_length=45, verbose_name="Nombre de servicio")
	glosa = models.TextField()
	margenNegocioPercent = models.FloatField(verbose_name="Porcentaje de margen de negocio")
	margenNegocioUF = models.FloatField( verbose_name="Margen de negocio en UF")
	responsableDelivery = models.CharField(max_length = 100 ,verbose_name="Responsable de Delivery")
	correoRespDelivery = models.CharField(max_length = 100 , verbose_name="Correo responsable de Delivery")
	linkNotaVenta = models.TextField()
	linkOC = models.TextField()
	linkPropuesta = models.TextField()
	linkEstimacion = models.TextField()
	negocio_id = models.ForeignKey(Negocio)
	estado = models.CharField(max_length=45)
	facturada = models.BooleanField(default=False)

class DocumentoTributario(models.Model):
	""" Tabla DocumentoTributario es la tabla que contiene la informacion de la factura realizada por hito. """
	numeroDoc = models.CharField(max_length=20, verbose_name="Numero Documento")
	tipoDoc = models.CharField(max_length=20, verbose_name="Tipo Documento")
	signo = models.CharField(max_length=45)
	fecha = models.DateField()
	totalCL = models.FloatField(verbose_name="Total Pesos")
	totalUF = models.FloatField(verbose_name="Total UF")
	observaciones = models.CharField(max_length=200)

class Hito(models.Model):	
	""" Tabla Hito, contiene los atributos de los hitos a ingresar por cada Nota de Venta, en caso de moficacion el campo 
	fechaEdicion contiene la fecha a la cual se quiere mover este hito, de la misma forma con el campo montoUFEdicion. 
	Esta tabla esta referenciada a Nota de Venta. """
	estado = models.CharField(max_length=20)
	fecha = models.DateField()
	comentarios = models.CharField(max_length=200)
	descripcion = models.CharField(max_length=200)
	montoCL = models.FloatField(verbose_name="Monto Pesos")
	montoUF = models.FloatField(verbose_name="Monto UF")
	montoDivisa = models.FloatField(verbose_name="Monto Divisa")
	divisa = models.CharField(max_length=45)
	tipoCambioDivisa = models.FloatField(verbose_name="Tipo de cambio Divisa")
	tipoCambioUF = models.FloatField(verbose_name="Tipo de cambio a UF")
	montoFacturado = models.FloatField(verbose_name="Monto Facturado")
	fechaEdicion = models.DateTimeField(null=True)
	notaVenta_id = models.ForeignKey(NotaVenta)
	montoUFEdicion = models.FloatField(verbose_name="Monto Pesos", default=0)

class DocumentoTributarioHito(models.Model):
	""" Esta tabla relaciona el DocumentoTributario con el Hito, ya que un Hito puede tener muchas facturass asociadas y
	 viceversa. """
	documentoTributario_id = models.ForeignKey(DocumentoTributario)
	hito_id = models.ForeignKey(Hito)

class LogHitos(models.Model):
	""" Esta tabla contiene la informacion de los Log que se crearan por cada Hito, esta tabla se actualiza una vez que se 
	ingresa un hito, se edita, se cambia la fecha y monto y se elimina. """
	fecha = models.DateField()
	tipo = models.CharField(max_length=45)
	hito_id = models.IntegerField(verbose_name="Hito Id")
	estado = models.BooleanField(default=False)#Si es false, no se ha gestionado aun el hito
	estadoHito = models.CharField(max_length=20, default=0)
	fechaHito = models.DateField(null=True)
	comentarios = models.CharField(max_length=200)
	descripcion = models.CharField(max_length=200)
	montoCL = models.FloatField(verbose_name="Monto Pesos")
	montoUF = models.FloatField(verbose_name="Monto UF")
	montoDivisa = models.FloatField(verbose_name="Monto Divisa")
	divisa = models.CharField(max_length=45)
	tipoCambioDivisa = models.FloatField(verbose_name="Tipo de cambio Divisa")
	tipoCambioUF = models.FloatField(verbose_name="Tipo de cambio a UF")
	montoFacturado = models.FloatField(verbose_name="Monto Facturado")
	estadoCompromiso = models.CharField(max_length=45, verbose_name="Estado de Compromiso")
	fechaEdicion = models.DateTimeField(null=True)
	notaVenta_id = models.IntegerField(verbose_name="NotaVenta Id")
	montoUFEdicion = models.FloatField(verbose_name="Monto Pesos", default=0)

class EjecutivoComercial(models.Model):
	""" Esta tabla contiene la informacion del ejecutivo comercial. """
	nombres = models.CharField(max_length=45)
	apellidos = models.CharField(max_length=45)
	email = models.EmailField(max_length=100)

class Participacion(models.Model):
	""" Esta tabla relaciona la Nota de Venta con EjecutivoComercial mediante la participacion que este tiene en la Nota
	de Venta. """
	porcentaje = models.FloatField()
	notaVenta_id = models.ForeignKey(NotaVenta)
	ejecutivoComercial_id = models.ForeignKey(EjecutivoComercial)

class BalanceAnual(models.Model):
	""" Esta tabla contiene datos para utilizarse en el balance anual, esta referenciada consigo misma. """
	totalNegocioUF = models.FloatField(verbose_name="Total del Negocio UF")
	anho = models.IntegerField(verbose_name="Annio")
	backlogInicio = models.FloatField(verbose_name="Inicio Backlog")
	totalAnualUF = models.FloatField(verbose_name="Total Anual UF")
	totalProxUF = models.FloatField(verbose_name="Total Proximo Annio UF")
	balanceAnt_id = models.ForeignKey('self')
	notaVenta_id = models.ForeignKey(NotaVenta)
