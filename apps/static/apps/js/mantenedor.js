var tipoServicio_arr = new Array("Proyecto de Software", "Proyectos IT", "Continuidad de Servicios", "Productos o Servicios de terceros");

var s_a = new Array();
s_a[0] = "";
s_a[1] = "Web y Otros|BPM|BI|SOA";
s_a[2] = "Proyectos IT|Consultoría";
s_a[3] = "Outsourcing de Aplicaciones|Outsourcing de Plataformas";

s_a[4] = "SW Oracle BD|SW Oracle BPM|SW Oracle BI|SW Oracle FM|SW Bizagi|SW Otros|HW Oracle|HW Otros|Otros Servicios";

function poblarSubTipo(tipoServicioElementId, subTipoServicioElementId) {

    var selectedtipoServicioIndex = document.getElementById(tipoServicioElementId).selectedIndex;

    var subTipoServicioElement = document.getElementById(subTipoServicioElementId);

    subTipoServicioElement.length = 0; // Fixed by Julian Woods
    subTipoServicioElement.options[0] = new Option('Selecciona subTipoServicio', '');
    subTipoServicioElement.selectedIndex = 0;

    var subTipoServicio_arr = s_a[selectedtipoServicioIndex].split("|");

    for (var i = 0; i < subTipoServicio_arr.length; i++) {
        subTipoServicioElement.options[subTipoServicioElement.length] = new Option(subTipoServicio_arr[i], subTipoServicio_arr[i]);
    }
}
//Función que llena los tipos de Nota de Venta existentes.
function poblarTipo(tipoServicioElementId, subTipoServicioElementId) {
    // given the id of the <select> tag as function argument, it inserts <option> tags
    var tipoServicioElement = document.getElementById(tipoServicioElementId);
    tipoServicioElement.length = 0;
    tipoServicioElement.options[0] = new Option('Selecciona tipoServicio', '');
    tipoServicioElement.selectedIndex = 0;
    for (var i = 0; i < tipoServicio_arr.length; i++) {
        tipoServicioElement.options[tipoServicioElement.length] = new Option(tipoServicio_arr[i], tipoServicio_arr[i]);
    }

    // Assigned all countries. Now assign event listener for the subTipoServicios.

    if (subTipoServicioElementId) {
        tipoServicioElement.onchange = function () {
            poblarSubTipo(tipoServicioElementId, subTipoServicioElementId);
        };
    }
}

//Función que llena los estados de los Hitos.

function poblarEstado(){
    var estados = ["Estado", "Tentativo","Comprometido", "En Riesgo OC", "En riesgo", "Facturado"];     
    var sel = document.getElementById('estado');
    for(var i = 0; i < estados.length; i++) {
        var opt = document.createElement('option');
        opt.innerHTML = estados[i];
        if (i == 0){
            opt.value = "";
        }
        else{
        opt.value = i;
        }
        sel.appendChild(opt);
    }
}

//Función que llena los tipos de documentos al facturar los Hitos.

function poblarTipoDoc(){
    var documentos = ["Documento", "Doc1","Doc2", "Doc3", "Doc4", "Doc5"];     
    var sel = document.getElementById('tipoDoc');
    for(var i = 0; i < documentos.length; i++) {
        var opt = document.createElement('option');
        opt.innerHTML = documentos[i];
        opt.value = i;
        sel.appendChild(opt);
    }
}

//Función que llena los grupos económicos en los clientes.

function poblarGrupoEconomico(){
    var grupos = ["Grupo Económico", "Grupo1","Grupo2", "Grupo3", "Grupo4", "Grupo5"];     
    var sel = document.getElementById('grupoEconomico');
    for(var i = 0; i < grupos.length; i++) {
        var opt = document.createElement('option');
        opt.innerHTML = grupos[i];
        opt.value = i;
        sel.appendChild(opt);
    }
}

//Función que llena los grupos económicos en los clientes.

function poblarSector(){
    var sectores = ["Sector", "Sector1","Sector2", "Sector3", "Sector4", "Sector5"];     
    var sel = document.getElementById('sector');
    for(var i = 0; i < sectores.length; i++) {
        var opt = document.createElement('option');
        opt.innerHTML = sectores[i];
        opt.value = i;
        sel.appendChild(opt);
    }
}