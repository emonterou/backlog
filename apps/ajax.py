from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register

@dajaxice_register
def updatecombo(request, option):
	dajax = Dajax()
	options = [['Arica', 'Parinacota'],
               ['Iquique'],
               ['Antofagasta', 'El Loa', 'Tocopilla'],
               ['Chanaral','Copiapo', 'Huasco'],
               ['Elqui','Limari','Choapa'],
               ['Valparaiso', 'Petorca', 'Los Andes', 'San Felipe', 'Quillota', 'San Antonio', 'Isla de Pascua'],
               ['Santiago', 'Cordillera', 'Melipilla', 'Talagante', 'Maipo', 'Chacabuco'],
               ['Cachapoal', 'Colchagua', 'Cardenal Caro'],               
               ['Curico', 'Talca', 'Linares', 'Cauquenes'],
               ['Nuble', 'Biobio', 'Concepcion', 'Arauco'],
               ['Malleco', 'Cautin'],
               ['Ranco', 'Valdivia'],
               ['Osorno', 'Llanquihue', 'Chiloe', 'Palena'],
               ['Capitan Prat', 'Aysen', 'Coyhaique', 'General Carrera'],
               ['Ultima Esperanza', 'Magallanes', 'Tierra del Fuego', 'Antartica Chilena']]
	out = []
	for option in options[int(option)]:
		out.append("<option value='#'>%s</option>" % option)

	dajax.assign('#combo2', 'innerHTML', ''.join(out))
	return dajax.json()